<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>App</title>
        
        <!-- Bootstrap -->
        <link href="{{asset('/')}}vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="{{asset('/')}}vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{asset('/')}}vendors/animate/animate.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Custom Theme Style -->
        @stack('csshead')
        <link href="{{asset('/')}}build/css/custom.min.css" rel="stylesheet">
    </head>
    <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{url('/')}}/admin" class="site_title"<span>Quản lý Bán Hàng </span></a>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
             <div class="profile">
               <div class="profile_info text-center">
                 <h1> {{ Auth::user()->name }} </h1>
               </div>
             </div>
            <!-- /menu profile quick info -->
            <br/>
          <div class="clearfix"></div>
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3> QUẢN LÝ XUẤT/NHẬP</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-archive" aria-hidden="true"></i> HÀNG HOÁ <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('/')}}/admin/hanghoa">Hàng Hoá</a></li>
                      <li><a href="{{url('/')}}/admin/nhomhang">Nhóm Hàng </a></li>
                      <li><a href="{{url('/')}}/admin/donvitinh">Đơn Vị Tính</a></li>
                      <li><a href="{{url('/')}}/admin/quycach">Quy Cách</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-edit"  aria-hidden="true"></i> KHO <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('/')}}/admin/kho">Kho</a></li>
                      <li><a href="{{url('/')}}/admin/kho/nhapkho">Nhập</a></li>                      
                    </ul>
                  </li>
                  <li><a href="{{url('/')}}/admin/khachhang"><i class="fa fa-desktop"></i> KHÁCH HÀNG  <span class="fa fa-chevron-down"></span></a>                    
                  </li>
                  @if(Auth::user()->role=='admin')
                  <li><a href="{{url('/')}}/admin/nhanvien"><i class="fa fa-table"></i> NHÂN VIÊN <span class="fa fa-chevron-down"></span></a>
                  </li>
                  @endif
                  <li><a href="{{url('/')}}/admin/tuyenduong"><i class="fa fa-table"></i> TUYẾN ĐƯỜNG <span class="fa fa-chevron-down"></span></a>
                  </li>
                </ul>
              </div>
              <div class="menu_section">
                <h3>THỐNG KÊ & BÁO CÁO</h3>
                <ul class="nav side-menu">
                  @if(Auth::user()->role=='admin')
                  <li><a href="{{url('/')}}/admin/doanhthu"><i class="fa fa-usd"></i>DOANH THU<span class="fa fa-chevron-down"></span></a>
                    <!-- <ul class="nav child_menu">
                      <li><a href="e_commerce.html">E-commerce</a></li>
                      <li><a href="projects.html">Projects</a></li>
                      <li><a href="project_detail.html">Project Detail</a></li>
                      <li><a href="contacts.html">Contacts</a></li>
                      <li><a href="profile.html">Profile</a></li>
                    </ul> -->
                  </li>
                @endif
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a>
               <i class="fa fa-circle"></i>
              </a>
              <a>
               <i class="fa fa-circle"></i>
              </a>
              <a>
               <i class="fa fa-circle"></i>
              </a>
              <a>
               <i class="fa fa-circle"></i>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li> <a href="{{url('/')}}/logout" class="btn btn-app"> <i class="fa fa-sign-out text-danger " aria-hidden="true"></i>THOÁT</a></li>
                @if(Auth::user()->role=='admin')
                <li ><a href="{{url('/')}}/admin/trahang" class="btn btn-app"><i class="fa fa-ship " aria-hidden="true"></i>  TRẢ HÀNG </a></li>
                @endif
                <li ><a href="{{url('/')}}/admin/donhang" class="btn btn-app"><i class="fa fa-book text-success" aria-hidden="true"></i>  ĐƠN HÀNG </a></li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
           @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Tiện ích quản lý bán hàng - #7
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    <meta name="_token" content="{!!csrf_token()!!}">
    <!-- jQuery -->
    <script src="{{asset('/')}}vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="{{asset('/')}}vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="{{asset('/')}}vendors/fastclick/lib/fastclick.js"></script>


    @stack('jsbottom')  
    <!--Numberic-->
    <script src="/vendors/autoNumeric/autoNumeric.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{asset('/')}}js/app.js"></script>
    <script src="{{asset('/')}}build/js/custom.min.js"></script> 
    </body>
</html>

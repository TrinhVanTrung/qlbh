@extends('index')

@push('csshead')

   <!-- PNotify -->
    <link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    
        <!-- Datatables -->
    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
           <!-- Select2 -->
    <link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

       

@endpush
@section('content')
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                  <h2>DOANH THU </h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                    
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">                                     
                <div class="clearfix"></div>
                   <div class="">
                     <div class="control-group">
                            <div class="control">
                              <div class="col-md-3 xdisplay_inputx form-group has-feedback">
                                <input type="text" class="form-control " id="single_cal2" >
                                <span class="fa fa-calendar-o fa-5x form-control-feedback right" aria-hidden="true"></span>     
                              </div>
                            </div>
                            <div class="control">

                                <div class="form-group xdisplay_inputx form-group has-feedback">                                  
                                  <div class="col-md-3"> 
                                    <input type="hidden"  id='id_nv' value="@if(Auth::user()->role=='user'){{Auth::user()->id}}@endif">                                    
                                    <select name='cmbNv' id='cmbNV' class="form-control has-feedback-left"></select><span class="fa fa-user fa-4x form-control-feedback right text-red" aria-hidden="true"></span>
                                  </div>                                  
                                </div>
                            </div>

                            <button type="button" class="btn btn-default blue" id='btn-fill' data-toggle="tooltip" title="Xem!"> <i class="fa fa-newspaper-o"></i> Xem</button>
                            <!-- <button type="button" class="btn btn-default green" id='btn-in' data-toggle="tooltip" title="In Đơn hàng theo ngày và 
                            nhân viên!"> <i class="fa fa-print"></i> In</button>     -->                       
                      </div>                      
                   </div> 
                   <div class="clearfix"></div> 
                  <div class="col-md-6">
                      <h2 class="title">Doanh thu theo hóa đơn </h2>
                     <table class="table table-hover table-striped table-bordered" id='datatable_theohoadon'>
                      <thead>                      
                        <th>Khách hàng</th>
                        <th>Tuyến</th>
                        <th>NV</th>
                        <th>Thành tiền</th>
                      </thead>
                      <tfoot> 
                       <tr>
                          <td colspan="3" class="text-right">Tổng tiền: </td>
                          <td class="text-right"> <strong id='tongtien_theohoadon'></strong> </td>
                       </tr>                     
                      </tfoot>
                      <tbody>                       
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-6"> 
                    <h2 class="title">Danh sách trả</h2>                   
                    <table class="table table-hover table-striped table-bordered" id='datatable_trahang'>
                      <thead>                      
                        <th>Khách hàng</th>
                        <th>Tuyến</th>
                        <th>NV</th>
                        <th>Thành tiền</th>
                      </thead>
                      <tfoot> 
                       <tr>
                          <td colspan="3" class="text-right">Tổng tiền: </td>
                          <td class="text-right red"> <strong id='tongtien_trahang'></strong> </td>
                       </tr>                     
                      </tfoot>
                      <tbody>                       
                      </tbody>
                    </table>
                  </div>                 
                  <!-- end table data -->
                  <div class="clearfix"></div>
                  <div class="">
                    <h2 class="text-uppercase">doanh thu thực tế : <strong id='doanhthuthucte'></strong></h2>
                  </div>
                </div>
              </div>
            </div>                    

          </div>
@stop
@push('jsbottom')

     <!-- PNotify -->
    <script src="/vendors/pnotify/dist/pnotify.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.confirm.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
    <!-- Datatables -->
    <script type="text/javascript" src="/public/js/ajax/ajax-doanhthu.js"></script>    
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
     
    <!-- Select2 -->
    <script src="/vendors/select2/dist/js/select2.full.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="/js/moment/moment.min.js"></script>
    <script src="/js/datepicker/daterangepicker.js"></script>    
@endpush    

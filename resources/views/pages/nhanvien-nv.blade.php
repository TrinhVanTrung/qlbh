@extends('index-nv')

@push('csshead')

   <!-- PNotify -->
    <link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    
        <!-- Datatables -->
    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
           <!-- Select2 -->
    <link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

@endpush
@section('content')
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                  <h2>ĐƠN HÀNG  <i class="fa fa-calendar"> </i> <span id='today' class=""></span></h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                    
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">                      
                  <table class="table" id='datatable'>
                    <thead>                      
                      <th>Khách hàng</th>
                      <th>Tuyến</th>
                      <th>Tác vụ</th>
                    </thead>
                    <tfoot>                      
                    </tfoot>
                    <tbody>
                       @foreach($data as $key=>$u)
                       <tr>                         
                         <td>{{$u->KH_TEN}}</td>
                         <td>{{$u->TD_TEN}}</td>
                         <td>
                         <a href="{{url('/')}}/nhanvien/banhang/{{$u->DH_ID}}" class="btn btn-default "" ><i class="fa fa-eye"></i></a></td>
                        </tr>
                       @endforeach
                    </tbody>
                  </table>
                  

                </div>
              </div>
            </div>                    

          </div>
@stop
@push('jsbottom')

     <!-- PNotify -->
    <script src="/vendors/pnotify/dist/pnotify.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.confirm.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
    <!-- Datatables -->
    <script type="text/javascript" src="/public/js/ajax/ajax-nv-nhanvien.js"></script>    
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <!-- Select2 -->
    <script src="/vendors/select2/dist/js/select2.full.min.js"></script>
    <!--Numberic-->
    <script src="/vendors/autoNumeric/autoNumeric.js"></script>

@endpush    

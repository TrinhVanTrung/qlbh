@extends('index-nv')

@push('csshead')
    <!-- PNotify -->
    <link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    
        <!-- Datatables -->
    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
           <!-- Select2 -->
    <link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">


@endpush

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> ĐƠN HÀNG HÔM NAY <button type="button" class="btn btn-default" id='today'  @if(isset($data[0])) value='{"ID_KH":{{$data[0]->ID_KH}},"ID_TD":{{$data[0]->ID_TD}} }' @endif ></button></h3>
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">                   
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Tuyến, Khách Hàng</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form id="frmBanHang" name="frmBanHang" class="form-horizontal" novalidate="">
                                 <div class="form-group">
                                      <label for="cmbTuyenduong"  class="col-xs-3 control-label">TUYẾN <span class='text-danger'>(*)</span></label>
                                      <div class="col-xs-9">
                                        <select name="cmbTuyenduong" id="cmbTuyenduong" class="form-control">
                                        </select>
                                      </div>
                                  </div>
                                  <div class="form-group">
                                      <label for="cmbKhachhang"  class="col-xs-3 control-label">KH <span class='text-danger'>(*)</span></label>
                                      <div class="col-xs-9">
                                        <select name="cmbKhachhang" id="cmbKhachhang" class="form-control">
                                          <option value=''>Chưa chọn</option>
                                        </select>
                                      </div>
                                  </div>
                                  @if(isset($data[0]))
                                  <div class="form-group">
                                    <button class="btn btn-danger" type='button' id="btn-huy"> <i class="fa fa-close"></i> Hủy</button>
                                  </div>
                                  @endif
                      </form>
                  </div>
                </div>
              </div>
              
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Chọn hàng hóa</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <form class="form-horizontal">
                      <div class="form-group">
                       <div class="col-xs-12">
                         <label for="txtTenhang">Hàng: <span class='text-danger'>(*)</span></label>
                          <select name="cmbHanghoa" id="cmbHanghoa" class="form-control">
                                        </select>
                       </div>
                       </div>
                       <div class="form-group">
                         <div class="col-xs-6">
                          <label for="txtSoluong">SL: <span class='text-danger'>(*)</span></label>
                         <input type="text" name="" id="txtSoluong" class="form-control"/>
                         </div>
                         <div class="col-xs-6">
                          <label for="txtDongia">Giá: <span class='text-danger'>(*)</span></label>
                         <input type="text" name="" id='txtDongia'  class="form-control"/>
                         </div>
                       </div>
                    </form>
                    <div class="col-xs-12">
                       <button type='button' class="btn btn-success" id='btnAdd' >Thêm</button>  
                       </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Danh sách bán</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover table-striped table-bordered display no-wrap " id="datatable" width="100%">
                      <thead>
                        <tr>
                          <th>HÀNG</th>
                          <th>SỐ LƯỢNG</th>
                          <th>ĐƠN GIÁ</th>
                          <th>SỬA</th>
                        </tr>
                      </thead>                      
                      <tbody>
                      <?php $tongtien="";?>
                        @foreach($data as $u)
                        <?php $tongtien+= ($u->DONGIA*$u->SOLUONG);?>
                        <tr>
                          <td class="col-md-3"><strong>{{$u->HH_TEN}} -  {{$u->QC_TEN}}</strong></td>
                          <td>{{$u->SOLUONG}}</td>
                          <td > <strong class="tiente" >{{$u->DONGIA}}</strong></td>
                          <td> <button class="btn open-modal" value="{{$u->CTDH_ID}}"> <i class="fa fa-pencil text-primary"></i> </button><button class="btn delete" value="{{$u->CTDH_ID}}"> <i class="fa fa-close text-danger "></i> </button> </td>
                        </tr>
                        @endforeach

                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="4">Tổng:  <strong class="text-danger tiente">{{$tongtien}}</strong>  </td>
                          
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
              </div>
              
            </div>
</div>
 <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="banhangModal" tabindex="-1" role="dialog" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">BÁN HÀNG</h4>
                        </div>
                        <div class="modal-body">
                          <form action="" class="fron-horizontal">
                            <div class="form-group">
                              <div class="col-xs-12">
                                <lable for='txtTenhang'>TÊN:</lable>
                                 <input type="text" name="txtTenhang" id='txtTenhang' value="" class='form-control' disabled='disabled'>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-xs-6">
                                <lable>SL:</lable>
                                 <input type="text" name="txtSoluong" id='txtSoluongedit' value="" class='form-control'>
                              </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                                  <lable>GIÁ:</lable>
                                 <input type="text" name="txtDongia" id='txtDongiaedit' value="" class='form-control'>
                              </div>
                            </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-6">
                              <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                
                              </div>
                            </div>
                            <div class="col-md-6">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Lưu</button>
                            <input type="hidden" id="chitietdonhang_id" name="chitietdonhang_id" value="0">  
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--modal alert delete-->
            <div class="modal fade" tabindex="-1" role="dialog" id="delModal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">
                    <p>One fine body&hellip;</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger btnXoa" id=""   >Xoá đi</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

@stop
@push('jsbottom')

     <!-- PNotify -->
    <script src="/vendors/pnotify/dist/pnotify.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.confirm.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
    <!-- Datatables -->
    <script type="text/javascript" src="/public/js/ajax/ajax-banhang.js"></script>    
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <!-- Select2 -->
    <script src="/vendors/select2/dist/js/select2.full.min.js"></script>


@endpush    


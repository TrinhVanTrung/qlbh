@extends('index')
@push('csshead')
    <!-- PNotify -->
    <link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
        <!-- Datatables -->
    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
           <!-- Select2 -->
    <link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
@endpush

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>NHÂN VIÊN <small></small></h3>
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                      <button id="btn-add" name="btn-add" class="btn btn-success" ><i class="fa fa-plus "> Thêm </i></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Danh sách</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover table-striped table-bordered" id="datatable">
                      <thead>
                        <tr>
                          <th>TÊN</th>
                          <th>Email</th>
                          <th>SĐT</th>
                          <th>Role</th>
                          <th>Pass</th>
                          <th>TÁC VỤ</th>                          
                        </tr>
                      </thead>
                      <tfoot>
                        
                      </tfoot>
                      <tbody>
                        @foreach($data as $u)
                        <tr>
                          <td class="col-md-2"><strong>{{$u->name}}</strong></td>
                          <td class="col-md-2">{{$u->email}}</td>
                          <td class="col-md-2 " >0{{$u->phone}}</td>
                          <td class="col-md-2" >{{$u->role}}</td>
                          <td class="col-md-2"> <button class="btn changepass" value="{{$u->id}}">Đổi pass</button></td>
                          <td> <button class="btn open-modal" value="{{$u->id}}"> <i class="fa fa-pencil text-primary"></i> </button> 
                               <button class="btn delete" value="{{$u->id}}"> <i class="fa fa-close text-danger "></i> </button>
                          </td>                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
</div>
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="nhanvienModal" tabindex="-1" role="dialog" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">NHÂN VIÊN</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmNhanVien" name="frmNhanVien" class="form-horizontal" novalidate="">
                                <div class="form-group">
                                    <label for="txtTen" class="col-sm-3 control-label">Tên: <span class="text-danger">*</span> </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txtTen" name="txtTen" placeholder="" value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txtEmail" class="col-sm-3 control-label">Email: <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="abc@demo.com" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txtMatKhau" class="col-sm-3 control-label">Mật Khẩu: <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="password" class="form-control" id="txtMatKhau" name="txtMatKhau" placeholder="" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txtSDT" class="col-sm-3 control-label">SỐ ĐT: <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="txtSDT" name="txtSDT" placeholder="" value="0">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txtRole" class="col-sm-3 control-label">R: <span class="text-danger">*</span></label>
                                    <div class="col-sm-9">
                                       <select class="form-control" name='cmbRole' id='cmbRole'>
                                         <option value="">Chọn</option>
                                         <option value="user">Nhân viên</option>
                                         <option value="kho">Kho</option>                                         
                                       </select>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-6">
                              <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                
                              </div>
                            </div>
                            <div class="col-md-6">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Lưu</button>
                            <input type="hidden" id="nhanvien_id" name="nhanvien_id" value="0">  
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--modal alert delete-->
            <div class="modal fade" tabindex="-1" role="dialog" id="delModal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">
                    <p>One fine body&hellip;</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger btnXoa" id=""   >Xoá </button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

@stop
@push('jsbottom')

     <!-- PNotify -->
    <script src="/vendors/pnotify/dist/pnotify.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
        <!-- Datatables -->
    <script type="text/javascript" src="/public/js/ajax/ajax-nhanvien.js"></script>    
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
  <!-- Select2 -->
    <script src="/vendors/select2/dist/js/select2.full.min.js"></script>
@endpush
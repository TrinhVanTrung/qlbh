@extends('index')
@push('csshead')
    <!-- PNotify -->
    <link href="{{asset('/')}}vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="{{asset('/')}}vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="{{asset('/')}}vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    <!-- Datatables -->
    <link href="{{asset('/')}}vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('/')}}vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
       <!-- Select2 -->
    <link href="{{asset('/')}}vendors/select2/dist/css/select2.min.css" rel="stylesheet">

@endpush

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>HÀNG HOÁ <small></small></h3>
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                      <button id="btn-add" name="btn-add" class="btn btn-success" ><i class="fa fa-plus "> Thêm </i></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2> Bảng </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover table-striped table-bordered" id="datatable">
                      <thead>
                        <tr>
                          <th>NHÓM</th>
                          <th>TÊN</th>
                          <th>QUY CÁCH</th>
                          <th>ĐVT</th>
                          <th>TÁC VỤ</th>
                          
                        </tr>
                      </thead>
                      <tfoot>
                      </tfoot>
                      <tbody id='list'>
                        @foreach($data as $hh)
                        <tr id="task{{$hh->HH_ID}}">
                          <td class="col-md-1">{{$hh->NH_TEN}}</td>
                          <td class="col-md-4">{{$hh->HH_TEN}}</td>
                          <td class="col-md-2">{{$hh->QC_TEN}}</td>
                          <td class="col-md-1">{{$hh->DVT_TEN}}</td>
                          <td class="col-md-1"> <button class="btn btn-sm open-modal" value="{{$hh->HH_ID}}"> <i class="fa fa-pencil text-primary"></i> </button>  <button class="btn btn-sm delete" value="{{$hh->HH_ID}}"> <i class="fa fa-close text-danger "></i> </button> </td>                         
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
</div>
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="hanghoaModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" tabindex="-1">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">Form - HÀNG HOÁ</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmHangHoa" name="frmHangHoa" class="form-horizontal" >
                                <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="cmbNhomhang"  class="col-sm-3 control-label">Nhóm hàng</label>
                                      <div class="col-sm-9">
                                        <select name="cmbNhomhang" id="cmbNhomhang" class="select_nhomhang form-control"  style="width: 100%;">
                                        <option></option>
                                        </select>
                                      </div>
                                    </div>                                  
                                    <div class="form-group ">
                                    <label for="txtTen" class="col-sm-3 control-label">Tên</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control " id="txtTen" name="txtTen" placeholder="Tên" value="" required>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                  <label for="cmbQuycach" class="control-label col-sm-3 ">Quy cách</label>
                                  <div class="col-sm-9 ">
                                    <select class="select_quycach form-control" id="cmbQuycach"  name="cmbQuycach"  style="width: 100%;">
                                      <option></option>
                                    </select>
                                  </div>
                                </div>
                                    <div class="form-group">
                                      <label for="cmbDonvitinh"  class="col-sm-3 control-label">Đơn vị tính</label>
                                      <div class="col-sm-9">
                                        <select name="cmbDonvitinh" id="cmbDonvitinh" class="select_dvt form-control"  style="width: 100%;">
                                          <option></option>
                                        </select>
                                      </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-6">
                              <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                              </div>
                            </div>
                            <div class="col-md-6">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Lưu</button>
                            <input type="hidden" id="hanghoa_id" name="hanghoa_id" value="0">  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--modal alert delete-->
            <div class="modal fade"  role="dialog" id="delModal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">
                    <p>One fine body&hellip;</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger btnXoa" id=""   >Xóa</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

@stop
@push('jsbottom')
  
     <!-- PNotify -->
    <script src="{{asset('/')}}vendors/pnotify/dist/pnotify.js"></script>
    <script src="{{asset('/')}}vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="{{asset('/')}}vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
        <!-- Datatables -->
    <script src="{{asset('/')}}vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('/')}}vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="{{asset('/')}}vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <!-- Select2 -->
    <script src="{{asset('/')}}vendors/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript" src="{{asset('/')}}js/ajax/ajax-hanghoa.js"></script>
@endpush

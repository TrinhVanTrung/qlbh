@extends('index')

@push('csshead')

   <!-- PNotify -->
    <link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    
        <!-- Datatables -->
    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
           <!-- Select2 -->
    <link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
   
@endpush
@section('content')
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                  <h2>ĐƠN HÀNG</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                    
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">                     
                @foreach($chuaduyet as $key => $u)
                <div class="col-md-3">
                <div class="alert alert-success alert-dismissible " role="alert">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <strong>Đơn hàng  <?php    $d= explode('-', explode(' ', $u->NGAYTAO)[0]); echo $d[2].'-'.$d[1].'-'.$d[0]; ?> chưa duyệt!</strong> 
                </div>                
                </div>
                @endforeach
                <div class="clearfix"></div>
                   <div class="">
                     <div class="control-group">
                            <div class="control">
                              <div class="col-md-3 xdisplay_inputx form-group has-feedback">
                                <input type="text" class="form-control " id="single_cal2" >
                                <span class="fa fa-calendar-o fa-5x form-control-feedback right" aria-hidden="true"></span>     
                              </div>
                            </div>
                            <div class="control">

                                <div class="form-group xdisplay_inputx form-group has-feedback">                                  
                                  <div class="col-md-3">                                     
                                    <select name='cmbNv' id='cmbNV' class="form-control has-feedback-left"></select><span class="fa fa-user fa-4x form-control-feedback right text-red" aria-hidden="true"></span>
                                  </div>                                  
                                </div>
                            </div>

                            <button type="button" class="btn btn-default blue" id='btn-fill' data-toggle="tooltip" title="Xem!"> <i class="fa fa-newspaper-o"></i> Xem</button>
                            <button type="button" class="btn btn-default green" id='btn-in' data-toggle="tooltip" title="In Đơn hàng theo ngày và 
                            nhân viên!"> <i class="fa fa-print"></i> In</button>
                            <a  class="btn btn-primary" id='btn-banhang' href="{{url('/')}}/admin/banhang" target=""> <i class="fa fa-shopping-bag"></i> Bán hàng</a>
                      </div>                      
                   </div>   
                  <table class="table table-hover table-striped table-bordered display no-wrap " id='datatable'>
                    <thead>                      
                      <th>Khách hàng</th>
                      <th>Tuyến</th>
                      <th>NV</th>
                      <th>Tác vụ</th>
                    </thead>
                    <tfoot>                      
                    </tfoot>
                    <tbody>
                       @foreach($data as $key=>$u)
                       <tr>                         
                         <td class="col-md-2">{{$u->KH_TEN}}</td>
                         <td class="col-md-3">{{$u->TD_TEN}}</td>
                         <td class="col-md-3">{{$u->name}}</td>
                         <td>
                         <button type='button' class="btn btn-info btn-xs btn-donhang" value="{{$u->DH_ID}}" data-toggle="tooltip" title="Xem chi tiết!"><i class="fa fa-eye"></i></button>
                         <button type="button" class="btn btn-default btn-xs  @if($u->DH_DUYET==1) {{'green'}}  @endif" data-toggle="tooltip" title="Trạng thái duyệt"> <i class="fa fa-check"></i></button>
                         <button type="button"  class="btn btn-xs btn-danger" id="btn-xoa-donhang" value="{{$u->DH_ID}}" data-toggle="tooltip" title="Xóa đơn hàng"> <i class="fa fa-close"></i></button>
                         </td>
                        </tr>
                       @endforeach
                    </tbody>
                  </table>
                  

                </div>
              </div>
            </div>                    

          </div>

          <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="donhangModal" tabindex="-1" role="dialog" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">ĐƠN HÀNG</h4>
                        </div>
                        <div class="modal-body">
                          <form action="" class="from-horizontal">
                            <table class="table table-hover table-striped table-bordered display no-wrap " id="datatable_chitiet" width="100%">
                               <thead>
                                 <th class="col-md-1">No</th>
                                 <th>Tên</th>
                                 <th class="col-md-1">SL</th>
                                 <th class="col-md-1">KHO</th>
                                 <th class="col-md-1">ĐG</th>
                                 <th class="col-md-2">$</th>
                                 <th class="col-md-2">Tác vụ</th>
                               </thead>
                               <tbody>                                 
                               </tbody>
                               <tfoot>                                 
                               </tfoot>
                            </table>                            
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-6">
                              <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                
                              </div>
                            </div>
                            <div class="col-md-6">
                            <button type="button" class="btn btn-primary" id="btn-duyet" value="">Duyệt</button>
                            <button type="button" class="btn" data-dismiss="modal"  value="">Thoát</button>
                            <input type="hidden" id="donhang_id" name="donhang_id" value="0">  
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--modal edit chitietdonhang-->
            <div class="modal fade" tabindex="-1" role="dialog" id="chitietdonhangModal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">FORM - EDIT</h4>
                  </div>
                  <div class="modal-body">
                    <form  class="form-horizontal" id='frmchitiet' novalidate="">                          
                             <div class="form-group">
                              <label  for="txtTen" class="col-md-1 control-label">Tên </label>
                               <div class="col-md-4">
                                 <input type="text" name="txtTen" id="txtTen" class="form-control" disabled="disabled">
                               </div> 
                               <label for="txtSoluong" class="col-md-1 control-label">SL: </label>
                                 <div class="col-md-2">
                                     <input type="text" name="txtSoluong" id="txtSoluong" class="form-control">
                                </div>
                                <label for="txtDongia" class="col-md-1 control-label">ĐG: </label>
                                 <div class="col-md-3">
                                   <input type="text" name="txtDongia" id="txtDongia" class="form-control tiente">
                                 </div> 
                            </div>                  

                    </form>
                  </div>
                  <div class="modal-footer">                    
                    <button type="button" class="btn btn-success  " id="btn-save-chitiet">Lưu</button>
                    <input type="hidden" id="chitietdonhang_id" name="chitietdonhang_id" value="0">  
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

@stop
@push('jsbottom')

     <!-- PNotify -->
    <script src="/vendors/pnotify/dist/pnotify.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.confirm.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
    <!-- Datatables -->
    <script type="text/javascript" src="/public/js/ajax/ajax-donhang.js"></script>    
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
     
    <!-- Select2 -->
    <script src="/vendors/select2/dist/js/select2.full.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="/js/moment/moment.min.js"></script>
    <script src="/js/datepicker/daterangepicker.js"></script>
@endpush    

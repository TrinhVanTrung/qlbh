<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>In Don Hang</title>
	<!-- Bootstrap -->
    <link href="{{asset('/')}}vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('/')}}vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet"> 

</head>
<body >
	<div class="" style="">
		 <div class="center">
		 	@foreach($data as $key => $u)
		 	<div class="" style="width:50%; height:700px; float:left; <?php if($key%2==0){echo "padding-right:20px;";}else{ echo "padding-left:20px;";} ?> " >
		 		<div style="width:70%; float:left;"> 
		 			<address>
					  <strong>CS: Dương Quang Xiếu.</strong><br>
						 Tầm Vu - Cần Thơ<br>							  
						 <abbr>SĐT:</abbr> (0901) 064-068 <br>
						 <abbr >Mã: </abbr> {{$u->DH_ID}} 
					  </address>
				</div>
	 			<div style="width:30%; float:right;" class="text-left">
	 				<ul class="list-unstyled">							  
						  <li> <abbr >KH: </abbr> <strong>{{$u->KH_TEN}}</strong></li>
						  <li> <abbr >ĐC: </abbr> <i> <strong> {{$u->KH_DIACHI}}</strong></i>  </li>
						  <li> <abbr >SĐT: </abbr>0<strong>{{$u->KH_SDT}}</strong></li>
						</ul>
	 			</div>		 		
		 		
		 		<div style='clear:both'>
		 			<h2 class="text-center" style="padding:0">Phiếu Xuất</h2>
		 			<table class=" display no-wrap table-bordered" style="width:100%">
		 				<thead>
		 					<th class="col-md-1 text-center">Stt</th>
		 					<th class="col-md-3 text-center">Tên</th>
		 					<th class="col-md-1 text-center">SL</th>
		 					<th class="col-md-3 text-center">ĐG</th>
		 					<th class="text-center">Thành Tiền</th>
		 				</thead>
		 				<tbody>			 				
			 				<?php 
			 				$tong=0;
			 				foreach ($chitiet[$u->DH_ID] as $key => $value): ?>
			 					<tr>
			 						<td style='padding:4px;' class="text-center">{{$key+1}}</td>
			 						<td style='padding:4px;'>{{$value->HH_TEN}} - {{$value->QC_TEN}}</td>
			 						<td style='padding:4px;' class="text-center">{{$value->SOLUONG}}</td>
			 						<td style='padding:4px;' class="tiente text-right">{{$value->DONGIA}}</td>
			 						<td style='padding:4px;' class="tiente text-right">{{$value->SOLUONG * $value->DONGIA }}</td>
			 					</tr>	
			 				<?php 
			 				$tong+=$value->SOLUONG * $value->DONGIA;
			 				endforeach ?>
		 				</tbody>
		 				
		 				<tfoot>
		 					<th colspan="4" class="text-right"><strong >Tổng:</strong></th>
		 					<th class="text-right" style='padding:4px;'><strong class="tiente">{{$tong}}</strong></th>
		 				</tfoot>
		 			</table>
		 		</div>
		 	<div class="text-right" style="height:90px;">
		 			<?php 
		 				$d = explode(' ' , $u->NGAYTAO);
		 				$d = explode('-', $d[0]);
		 			 ?>
		 		 <i>Cần Thơ, ngày <?php echo $d[2]; ?>  tháng <?php echo $d[1]; ?> năm  <?php  echo $d[0]; ?></i>	
		 	</div>	
		 	<div>
		 		<i>** Ghi Chú: </i><br>
		 		
		 	</div>
		 	</div> <!-- vong lap cho nay -->

		 	@endforeach				 	
		 	
		 </div>
	</div>
	<!-- jQuery -->
	<script src="{{asset('/')}}vendors/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap -->
	<script src="{{asset('/')}}vendors/bootstrap/dist/js/bootstrap.min.js"></script>
	 <!--Numberic-->
    <script src="/vendors/autoNumeric/autoNumeric.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="{{asset('/')}}js/app.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			setautoNumeric('.tiente','tiente');
		});

	</script>
	<script>
	window.print();	
	setTimeout(function () { window.close(); }, 100);
	</script>

</body>
</html>
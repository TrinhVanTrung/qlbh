@extends('index')

@push('csshead')

   <!-- PNotify -->
    <link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    
        <!-- Datatables -->
    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
           <!-- Select2 -->
    <link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
   
@endpush
@section('content')
          <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <div class="x_panel tile">
                <div class="x_title">
                  <h2 class="text-uppercase"> xừ lý hàng trả</h2>
                  <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>                    
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                  </ul>
                  <div class="clearfix"></div>
                </div>
                <div class="x_content">                                    
                <div class="clearfix"></div>
                   <div class="">
                      <form class="form-horizontal">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txtMadonhang" class="col-md-3 control-label"> Mã Hàng <span class="red">*</span></label>
                              <div class="col-md-7">
                                <input type="text" name="txtMadonhang" id='txtMadonhang' class="form-control">                             
                              </div>
                              
                              <button type="button" class="btn btn-primary" id='btn-search'> <i class="fa fa-search"></i></button>  
                            </div>                            
                          </div>
                          <div class="col-md-6">
                            <p class="tex-success"> Mã Hàng: <span id='id_DH' class="text-primary"></span> </p>   
                          </div>

                          <div class="clearfix"></div>
                          <div class="col-md-6">
                            <div class="form-group">
                               <label class="control-label col-md-2" for="cmbHanghoa">Trả gì <span class="red">*</span> </label>
                               <div class="col-md-6">
                                  <select class="form-control" style="width:100%" id="cmbHanghoa"></select>
                               </div> 
                               <label fop="txtSoluong" class="control-label col-md-2">SL <span class="red">*</span></label>
                               <div class="col-md-2">
                                 <input type="text" name="txtSoluong" id='txtSoluong' value="" class="form-control">
                               </div>
                                                                                           
                            </div>
                            <div class="form-group">
                                  <label for='txtGhichu' class="control-label col-md-2" > Ghi Chú  </label>
                                  <div class="col-md-10">
                                    <textarea class="form-control" id='txtGhichu' cols="4" rows="2"></textarea>
                                  </div>                                  
                                </div>
                          </div>
                          <div class="col-md-6">
                                <div class="form-group">
                                 <label fop="txtDongia" class="control-label col-md-2">DG <span class="red">*</span></label>
                                 <div class="col-md-4">
                                   <input type="text" name="txtDongia" id='txtDongia' value="" class="form-control">
                                 </div>                                  
                                  <button type="button" class="btn btn-danger" id='btn-trahang'>Trả hàng</button>
                                </div>
                                
                            </div>

                      </form>                     
                   </div>   
                  <table class="table table-hover table-striped table-bordered display no-wrap " id='datatable'>
                    <thead>                      
                      <th> Tên </th>
                      <th> Số Lượng</th>
                      <th> Đơn Giá </th>
                      <th> Thành Tiền </th>
                      <th>Tác vụ</th>
                    </thead>
                    <tfoot>                      
                    </tfoot>
                    <tbody>
                      
                    </tbody>
                  </table>
                  

                </div>
              </div>
            </div>                    

          </div>

          <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="donhangModal" tabindex="-1" role="dialog" >
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">ĐƠN HÀNG</h4>
                        </div>
                        <div class="modal-body">
                          <form action="" class="from-horizontal">
                            <table class="table table-hover table-striped table-bordered display no-wrap " id="datatable_chitiet" width="100%">
                               <thead>
                                 <th class="col-md-1">No</th>
                                 <th>Tên</th>
                                 <th class="col-md-1">SL</th>
                                 <th class="col-md-1">KHO</th>
                                 <th class="col-md-1">ĐG</th>
                                 <th class="col-md-2">$</th>
                                 <th class="col-md-2">Tác vụ</th>
                               </thead>
                               <tbody>                                 
                               </tbody>
                               <tfoot>                                 
                               </tfoot>
                            </table>                            
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-6">
                              <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                
                              </div>
                            </div>
                            <div class="col-md-6">
                            <button type="button" class="btn btn-primary" id="btn-duyet" value="">Duyệt</button>
                            <button type="button" class="btn" data-dismiss="modal"  value="">Thoát</button>
                            <input type="hidden" id="donhang_id" name="donhang_id" value="0">  
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--modal edit chitietdonhang-->
            <div class="modal fade" tabindex="-1" role="dialog" id="chitietdonhangModal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">FORM - EDIT</h4>
                  </div>
                  <div class="modal-body">
                    <form  class="form-horizontal" id='frmchitiet' novalidate="">                          
                             <div class="form-group">
                              <label  for="txtTen" class="col-md-1 control-label">Tên </label>
                               <div class="col-md-4">
                                 <input type="text" name="txtTen" id="txtTen" class="form-control" disabled="disabled">
                               </div> 
                               <label for="txtSoluong" class="col-md-1 control-label">SL: </label>
                                 <div class="col-md-2">
                                     <input type="text" name="txtSoluong" id="txtSoluong" class="form-control">
                                </div>
                                <label for="txtDongia" class="col-md-1 control-label">ĐG: </label>
                                 <div class="col-md-3">
                                   <input type="text" name="txtDongia" id="txtDongia" class="form-control tiente">
                                 </div> 
                            </div>                  

                    </form>
                  </div>
                  <div class="modal-footer">                    
                    <button type="button" class="btn btn-success  " id="btn-save-chitiet">Lưu</button>
                    <input type="hidden" id="chitietdonhang_id" name="chitietdonhang_id" value="0">  
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

@stop
@push('jsbottom')

     <!-- PNotify -->
    <script src="/vendors/pnotify/dist/pnotify.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.confirm.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
    <!-- Datatables -->
    <script type="text/javascript" src="/public/js/ajax/ajax-trahang.js"></script>    
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
     
    <!-- Select2 -->
    <script src="/vendors/select2/dist/js/select2.full.min.js"></script>

    <!-- bootstrap-daterangepicker -->
    <script src="/js/moment/moment.min.js"></script>
    <script src="/js/datepicker/daterangepicker.js"></script>
@endpush    

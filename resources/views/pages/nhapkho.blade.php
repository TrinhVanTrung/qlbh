@extends('index')
@push('csshead')
    <!-- PNotify -->
    <link href="/vendors/pnotify/dist/pnotify.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.buttons.css" rel="stylesheet">
    <link href="/vendors/pnotify/dist/pnotify.nonblock.css" rel="stylesheet">
    
    <!-- Datatables -->
    <link href="/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
       <!-- Select2 -->
    <link href="/vendors/select2/dist/css/select2.min.css" rel="stylesheet">

@endpush

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>NHẬP KHO <small></small></h3>
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                      <button id="btn-add" name="btn-add" class="btn btn-primary" >  <i class="fa fa-plus "> Thêm  </i></button>
                      <button id="btn-save-on" name="btn-save-on" class="btn btn-success" >  <i class="fa fa-save "> Lưu </i></button>                      
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="">
              <h3 class="pull-left"><i class="fa fa-key"></i>  <span class="text-primary" id='id_PN' value='0'></span></h3>
              <h3 class="pull-right"><i class="fa fa-calendar"> <span id='ngaythang'></span> </i></h3>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>DANH SÁCH HÀNG NHẬP</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover table-striped table-bordered" id="datatable">
                      <thead>
                        <tr>
                          <th>NHÓM</th>
                          <th>TÊN</th>
                          <th>SỐ LƯỢNG</th>
                          <th>**</th>
                        </tr>
                      </thead>
                      <tfoot>
                      </tfoot>
                      <tbody>
                        @foreach($data as $kho)
                        <tr>
                          <td class="col-md-1">{{$kho->NH_TEN}}</td>
                          <td class="col-md-4">{{$kho->HH_TEN}} - {{$kho->QC_TEN}}</td>
                          <td class="col-md-1">{{$kho->SOLUONG}}</td>
                          <td> 
                            <button class="btn btn-link btn-xs open-modal" value="{{$kho->HH_ID}}"> <i class="fa fa-edit text-success"></i> </button>
                            <button class="btn btn-link   btn-xs open-modal" value=""> <i class="fa fa-delete text-danger"></i> Lich sử Xuất </button>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
</div>
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="themhangModal" role="dialog" data-backdrop="static"  aria-hidden="true" tabindex='-1'>
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Chọn hàng nhập kho</h4>
                        </div>
                        <div class="modal-body">
                          <form action="" class="form-horizontal" name="frmThemHang" id='frmThemHang'>
                            <div class="col-md-8">
                                  <div class="form-group">
                                      <label for="cmbHanghoa"  class="col-xs-4 control-label">Tên hàng: </label>
                                      <div class="col-xs-8">
                                        <select name="cmbHanghoa" id="cmbHanghoa" class="select_hanghoa form-control" style="width: 100%;"> </select>
                                      </div>
                                    </div>                                  
                          </div>
                          <div class="col-md-4">
                            <div class="form-group ">
                                    <label for="txtSoluong" class="col-xs-4 control-label">SL:</label>
                                    <div class="col-xs-8">
                                        <input type="number" class="form-control " id="txtSoluong" name="txtSoluong" placeholder="" value="25" min="25" required>
                                    </div>
                          </div>
                        </div>
                        </form>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-6">
                              <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                              </div>
                            </div>
                            <div class="col-md-6">
                            <button type="button" class="btn btn-success" id="btn-save" value="add"> <i class="fa fa-plus"></i> Thêm</button>
                            <input type="hidden" id="hanghoa_id" name="hanghoa_id" value="0">  
                            </div>
                            
                            </div>
                        </div>
                    </div>
                
            </div>
            <!--modal alert delete-->
            <div class="modal fade"  role="dialog" id="delModal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">
                    <p>One fine body&hellip;</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger btnXoa" id=""   >Xoá đi</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

@stop
@push('jsbottom')
  
     <!-- PNotify -->
    <script src="/vendors/pnotify/dist/pnotify.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.buttons.js"></script>
    <script src="/vendors/pnotify/dist/pnotify.nonblock.js"></script>
    
        <!-- Datatables -->
    <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <!-- Select2 -->
    <script src="/vendors/select2/dist/js/select2.full.min.js"></script>
    <script type="text/javascript" src="/public/js/ajax/ajax-nhapkho.js"></script>
@endpush

@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>NHẬP KHO <small></small></h3>
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                      <button id="btn-add" name="btn-add" class="btn btn-primary" >  <i class="fa fa-plus "> Thêm  </i></button>
                      <button id="btn-save-on" name="btn-save-on" class="btn btn-success" >  <i class="fa fa-save "> Lưu </i></button>
                      
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div class="">
              <h3 class="pull-left"><i class="fa fa-key"></i>  <span class="text-primary" id='id_PN' value='{{$PN_ID}}'>{{$PN_ID}}</span></h3>
              <h3 class="pull-right"><i class="fa fa-calendar"> <span id='ngaythang'></span> </i></h3>
              
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>DANH SÁCH HÀNG NHẬP</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover table-striped table-bordered" id="datatable">
                      <thead>
                        <tr>
                          <th>NHÓM</th>
                          <th>TÊN</th>
                          <th>SỐ LƯỢNG</th>
                          <th>**</th>
                        </tr>
                      </thead>
                      <tfoot>
                      </tfoot>
                      <tbody>
                        @foreach($data as $chitietphieunhap)
                        <tr>
                          <td class="col-md-1">{{$chitietphieunhap->NH_TEN}}</td>
                          <td class="col-md-4">{{$chitietphieunhap->HH_TEN}} - {{$chitietphieunhap->QC_TEN}} </td>
                          <td class="col-md-1" id='sl{{$chitietphieunhap->ID_HH}}'>{{$chitietphieunhap->SOLUONG}}</td>
                          <td> 
                            <button class="btn   bg-primary open-modal" value="{{$chitietphieunhap->HH_ID}}"> <i class="fa fa-edit"></i> </button>
                            <button class="btn  delete" value="{{$chitietphieunhap->HH_ID}}"> <i class="fa fa-trash "></i>  </button>
                            
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
</div>
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="themhangModal" role="dialog" data-backdrop="static"  aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel"> Chọn hàng nhập kho</h4>
                        </div>
                        <div class="modal-body">
                          <form action="" class="form-horizontal" name="frmThemHang" id='frmThemHang'>
                            <div class="col-md-8">
                                  <div class="form-group">
                                      <label for="cmbHanghoa"  class="col-sm-4 control-label">Tên hàng: </label>
                                      <div class="col-sm-8 col-md-8">
                                        <select name="cmbHanghoa" id="cmbHanghoa" class="select_hanghoa form-control"  style="width: 100%;">
                                        <option></option>
                                        </select>
                                      </div>
                                    </div>                                  
                          </div>
                          <div class="col-md-4">
                            <div class="form-group ">
                                    <label for="txtSoluong" class="col-sm-4 control-label">SL:</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control " id="txtSoluong" name="txtSoluong" placeholder="" value="25" min="25" required>
                                    </div>
                          </div>
                        </div>
                        </form>
                        <div class="modal-footer">
                            <div class="col-md-6">
                              <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                              </div>
                            </div>
                            <div class="col-md-6">
                            <button type="button" class="btn btn-success" id="btn-save" value="add"> <i class="fa fa-plus"></i> Thêm</button>
                            <input type="hidden" id="hanghoa_id" name="hanghoa_id" value="0">  
                            </div>
                            
                            </div>
                        </div>
                    </div>
                
            </div>
            </div>
            <!--modal alert delete-->
            <div class="modal fade"  role="dialog" id="delModal"  data-backdrop="static" data-keyboard="false" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">
                    <p>One fine body&hellip;</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger btnXoa" id=""   >Xoá đi</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
    <script type="text/javascript" >
                 $(window).bind('beforeunload', function () {
                         return 'THÔNG BÁO!!!! NẾU BẠN CHUYỂN QUA TRANG KHÁC THÌ HỆ THỐNG SẼ KHÔNG LƯU LẠI THÔNG TIN BẠN VỪA THAO TÁC';

                   });
    </script>
@stop


@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> NHÀ CUNG CẤP <small></small></h3>
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                      <button id="btn-add" name="btn-add" class="btn btn-success" ><i class="fa fa-plus "> Thêm </i></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Danh sách</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover table-striped table-bordered" id="datatable">
                      <thead>
                        <tr>
                          <th>TÊN</th>
                          <th>SĐT</th>
                          <th>ĐỊA CHỈ</th>
                          <th>SỬA</th>
                          <th>XOÁ</th>
                        </tr>
                      </thead>
                      <tfoot>
                        
                      </tfoot>
                      <tbody>
                        @foreach($data as $u)
                        <tr>
                          <td class="col-md-4"><strong>{{$u->NCC_TEN}}</strong></td>
                          <td class="col-md-4"><strong>{{$u->NCC_SDT}}</strong></td>
                          <td class="col-md-6"><strong>{{$u->NCC_DIACHI}}</strong></td>
                          <td> <button class="btn open-modal" value="{{$u->NCC_ID}}"> <i class="fa fa-pencil text-primary"></i> </button> </td>
                          <td> <button class="btn delete" value="{{$u->NCC_ID}}"> <i class="fa fa-close text-danger "></i> </button> </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
</div>
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="nhacungcapModal" tabindex="-1" role="dialog" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">NHÀ CUNG CẤP</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmNhaCungCap" name="frmNhaCungCap" class="form-horizontal" novalidate="">
                                <div class="form-group">
                                    <label for="txtTen" class="col-sm-3 control-label">Tên:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txtTen" name="txtTen" placeholder="" value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txtSDT" class="col-sm-3 control-label">SĐT:</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="txtSDT" name="txtSDT" placeholder="" value="" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="txtDiachi" class="col-sm-3 control-label">Địa chỉ:</label>
                                    <div class="col-sm-9">
                                        <textarea name="txtDiachi" id="txtDiachi"  class="form-control" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-6">
                              <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                
                              </div>
                            </div>
                            <div class="col-md-6">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Lưu</button>
                            <input type="hidden" id="nhacungcap_id" name="nhacungcap_id" value="0">  
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--modal alert delete-->
            <div class="modal fade" tabindex="-1" role="dialog" id="delModal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">
                    <p>One fine body&hellip;</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger btnXoa" id=""   >Xoá đi</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

@stop

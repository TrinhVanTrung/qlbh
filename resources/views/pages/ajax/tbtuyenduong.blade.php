@section('content')
<div class="">
            <div class="page-title">
              <div class="title_left">
                <h3> TUYẾN ĐƯỜNG <small></small></h3>
              </div>
              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                      <button id="btn-add" name="btn-add" class="btn btn-success" ><i class="fa fa-plus "> Thêm </i></button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Danh sách</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table class="table table-hover table-striped table-bordered" id="datatable">
                      <thead>
                        <tr>
                          <th>TÊN</th>
                          <th>TÁC VỤ</th>
                         
                        </tr>
                      </thead>
                      <tfoot>
                        
                      </tfoot>
                      <tbody>
                        @foreach($data as $u)
                        <tr>
                          <td class="col-md-6"><strong>{{$u->TD_TEN}}</strong></td>
                          <td> <button class="btn open-modal" value="{{$u->TD_ID}}"> <i class="fa fa-pencil text-primary"></i> </button> 
                               <button class="btn delete" value="{{$u->TD_ID}}"> <i class="fa fa-close text-danger "></i> </button>
                          </td>                          
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
          </div>
</div>
            <!-- Modal (Pop up when detail button clicked) -->
            <div class="modal fade" id="tuyenduongModal" tabindex="-1" role="dialog" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 class="modal-title" id="myModalLabel">TUYẾN ĐƯỜNG</h4>
                        </div>
                        <div class="modal-body">
                            <form id="frmTuyenDuong" name="frmTuyenDuong" class="form-horizontal" novalidate="">
                                <div class="form-group">
                                    <label for="txtTen" class="col-sm-3 control-label">Tên:</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="txtTen" name="txtTen" placeholder="" value="" required>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <div class="col-md-6">
                              <div class="alert alert-warning alert-dismissible fade in hidden" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                
                              </div>
                            </div>
                            <div class="col-md-6">
                            <button type="button" class="btn btn-primary" id="btn-save" value="add">Lưu</button>
                            <input type="hidden" id="tuyenduong_id" name="tuyenduong_id" value="0">  
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!--modal alert delete-->
            <div class="modal fade" tabindex="-1" role="dialog" id="delModal">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Modal title</h4>
                  </div>
                  <div class="modal-body">
                    <p>One fine body&hellip;</p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Không</button>
                    <button type="button" class="btn btn-danger btnXoa" id=""   >Xoá đi</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

@stop

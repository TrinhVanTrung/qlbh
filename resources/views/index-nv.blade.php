<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>App</title>
        
        <!-- Bootstrap -->
        <link href="/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="/vendors/animate/animate.min.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- Custom Theme Style -->
        @stack('csshead')
        <link href="/build/css/custom.min.css" rel="stylesheet">
    </head>
    <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="{{url('/nhanvien')}}" class="site_title"> <span> NHÂN VIÊN  </span></a>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
            <div class="profile">              
              <div class="profile_info">                
                <h2> Chào, @if(Auth::check()){{ Auth::user()->name }} @endif</h2>             
                
              </div>
            </div>
            <!-- /menu profile quick info -->

          <div class="clearfix"></div>
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3> THÔNG TIN </h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-archive" aria-hidden="true"></i> HÀNG HOÁ <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('/')}}/nhanvien/hanghoa">Hàng Hoá</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-desktop"></i> KHÁCH HÀNG  <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{url('/')}}/nhanvien/khachhang">Người Mua</a></li>
                    </ul>
                  </li>
                  </li>
                  <li><a href="{{url('/')}}/nhanvien/tuyenduong"><i class="fa fa-table"></i> TUYẾN ĐƯỜNG <span class="fa fa-chevron-down"></span></a>
                  </li>
                </ul>
              </div>
              <div class="menu_section">
                <h3>THỐNG KÊ & BÁO CÁO</h3>
               <ul class="nav side-menu">
                 
                  <li><a href="{{url('/')}}/nhanvien/doanhthu"><i class="fa fa-usd"></i>DOANH THU<span class="fa fa-chevron-down"></span></a>
                    <!-- <ul class="nav child_menu">
                      <li><a href="e_commerce.html">E-commerce</a></li>
                      <li><a href="projects.html">Projects</a></li>
                      <li><a href="project_detail.html">Project Detail</a></li>
                      <li><a href="contacts.html">Contacts</a></li>
                      <li><a href="profile.html">Profile</a></li>
                    </ul> -->
                  </li>
                
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Thoát">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <ul class="nav navbar-nav navbar-right">
                <li> <a href="{{url('/')}}/logout" class="btn btn-app"> <i class="fa fa-sign-out text-danger " aria-hidden="true"></i>THOÁT</a></li>
                <li ><a href="{{url('/')}}/nhanvien/banhang" class="btn btn-app"><i class="fa fa-book text-success" aria-hidden="true"></i>  BÁN HÀNG </a></li>
                <li ><a href="{{url('/')}}/nhanvien" class="btn btn-app"><i class="fa fa-list text-primary" aria-hidden="true"></i>  ĐƠN HÀNG </a></li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
           @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Tiện ích quản lý bán hàng - #7
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>
    <meta name="_token" content="{!!csrf_token()!!}">
    <!-- jQuery -->
    <script src="/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="/vendors/fastclick/lib/fastclick.js"></script>
    @stack('jsbottom')  
                    <!--Numberic-->
    <script src="/vendors/autoNumeric/autoNumeric.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="/public/js/app.js"></script>
    <script src="/build/js/custom.min.js"></script> 
    </body>
</html>

@extends('layouts.app')

@section('content')
<div id="register" class="animate form registration_forma">
          <section class="login_content">
          @foreach($errors->all() as $error) <span> {{$error}} </span>  @endforeach 
            <form method="post" action="{{url('/')}}/register">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
              <h1>Create Account</h1>
              <div>
                <input type="text" name='name' id='name' class="form-control" placeholder="Username" required="" />
              </div>
              <div>
                <input type="text" name='phone' id='phone' class="form-control" placeholder="Phone" required="" />
              </div>
              <div>
                <input type="email"  name='email' id='email' class="form-control" placeholder="Email" required="" />
              </div>
              <div>
                <input type="password" name='password' id='password' class="form-control" placeholder="Password" required="" />
              </div>
              <div>
                <button type="submmit">Register</button>
              </div>
             
            </form>
          </section>
        </div>
@endsection

@extends('layouts.app')

@section('content')
<div class="animate form login_form">
          <section class="login_content">
            @foreach($errors->all() as $error) <span> {{$error}} </span>  @endforeach 
            <form method='post' action="{{url('/')}}/login">
                            
              <input type="hidden" name="_token" value="{{csrf_token()}}">
              <h1>ĐĂNG NHẬP</h1>
              <div class="form-group">
                <input type="text" class="form-control" name='email' id='email' placeholder="Email" required="" />
              </div>
              <div class="form-group">
                <input type="password" class="form-control" name='password' id='password' placeholder="Mật khẩu" required="" />
              </div>
              <div>
                <button type="submmit" class="btn btn-default">Đăng Nhập</button>
              </div>
              <div>
                 <div class="col-md-6">
                    <p>Nhân viên 1: </p>
                    <p>Email: demo@demo.com</p>
                    <p>Mật khẩu: demo123</p>
                    <hr>
                    <p>Nhân viên 2: </p>
                    <p>Email: demo2@demo.com</p>
                    <p>Mật khẩu: demo123</p>

                 </div>
                 <div class="col-md-6">
                    <p>Quản trị : </p>
                    <p>Email: admin@qlbh.com</p>
                    <p>Mật khẩu: admin123</p>
                    <hr>
                    <p>KHO : </p>
                    <p>Email: kho@demo.com</p>
                    <p>Mật khẩu: demo123</p>
                 </div>
              </div>  
             </div>             
              <div class="clearfix"></div>
            </form>
          </section>
</div>
@endsection

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuycachTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('quycach', function (Blueprint $table) {
            $table->string('QC_ID');
            $table->primary('QC_ID');
            $table->string('QC_TEN');
            $table->string('QC_GHICHU');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('quycach');
    }
}

<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','phone', 'role'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    function isAdmin($role)
    {
         // nhieu role, phan cach nhau boi dau  ,

        $role = explode('-',$role);
        $rt = false;
        foreach ($role as $key => $value) {
            
            if($this->role==$value)
            {
                $rt = true;
            }           

        }
        return $rt;
    }
}

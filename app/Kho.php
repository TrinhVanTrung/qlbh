<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kho extends Model
{
    //
    protected $table='kho';
    protected $fillable=['ID_HH','SOLUONG'];
    public $primaryKey ='ID_HH';
    public $incrementing = false;
}

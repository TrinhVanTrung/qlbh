<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HangHoa extends Model
{
    //
    protected $table="hanghoa";
    protected $fillable=['HH_ID','HH_TEN','ID_DVT','ID_NH','ID_QC'];
    public $primaryKey='HH_ID';

    
    
}

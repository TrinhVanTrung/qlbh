<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonViTinh extends Model
{
    //
    protected $table="donvitinh";
    protected $fillable=['DVT_ID','DVT_TEN'];
    public $primaryKey='DVT_ID';
    public $incrementing = false;
    
}

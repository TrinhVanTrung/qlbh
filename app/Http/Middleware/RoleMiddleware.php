<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class RoleMiddleware
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(Auth::check())        {
             
           if(!$request->user()->isAdmin($role))
                    {
                        return redirect('/');
                    }  

        }
        else
        {
            return redirect()->guest('/login');
        }
                   
        return $next($request);
    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Carbon;

use Response;

class AdminController extends Controller
{
    function index()
    {
      $date= Carbon\Carbon::today();
        $data['data'] = DB::table('donhang')
                        ->leftJoin('chitietdonhang','donhang.DH_ID','=','chitietdonhang.ID_DH')                        
                        ->leftJoin('users','users.id','=','donhang.ID_NV')
                        ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                        ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')
                        ->where('NGAYTAO',$date)
                        ->groupBy('users.id')                     
                        ->groupBy('khachhang.KH_ID')
                        ->get();
        $data['chuaduyet'] = DB::table('donhang')->where('DH_DUYET',0)->where('NGAYTAO','<',$date)->groupBy('NGAYTAO')->get();
        return view('pages.donhang',$data);
    }
    
    
    function create(Request $request)
    {
        $e =  DB::table('users')->where('email',$request['email'])->get();
        $data= ['success'=>'ok'];   
        if(count($e)==1)
        {
            $data= ['error'=>'idisset']; 
        }
        else
        {
             DB::table('users')->insert([
            'name'      =>$request['name'],
            'phone'     =>$request['phone'],
            'email'     =>$request['email'],
            'password'  =>bcrypt($request['password']),
            'role'      =>$request['role'],
            ]);
        }
                
        return Response::json($data);    
    }
    function update(Request $request,$id)
    {
        DB::table('users')->where('id',$id)->update([
            'name'  =>$request['name'],
            'phone' =>$request['phone'],
            'email' =>$request['email'],
            'role'  =>$request['role'],
            ]);
        $data = DB::table('users')->where('role','user')->orwhere('role','kho')->get();    
        return Response::json($data);
    }

    function changepass(Request $request,$id)
    {
        DB::table('users')->where('id',$id)->update([
            'password'  =>bcrypt($request['password'])]);
        $data = DB::table('users')->where('role','user')->orwhere('role','kho')->get();    
        return Response::json($data);
    }
      
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Response;

class TuyenDuongController extends Controller
{
    
    function create(Request $request)
    {
        $e =  DB::table('tuyenduong')->where('TD_TEN',$request['TD_TEN'])->get();
        $data= ['success'=>'ok'];   
        if(count($e)==1)
        {
            $data= ['error'=>'idisset']; 
        }
        else
        {
            DB::table('tuyenduong')->insert([
            'TD_TEN'=>$request['TD_TEN'],
            ]);
        }
        
        
        return Response::json($data);    
    }
    function update(Request $request,$id)
    {
        DB::table('tuyenduong')->where('TD_ID',$id)->update([
            'TD_TEN'=>$request['TD_TEN'],
            ]);
        $data = DB::table('tuyenduong')->get();    
        return Response::json($data);
    }
      
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Response;

use App\HangHoa;

class HangHoaController extends Controller
{
    //
    function __construct()
    {
        
    }
    public function create(Request $request)
    {
        $id= HangHoa::find($request->get('QC_ID'));
        if($id)
        {
            $data= ['error'=>'idisset'];
        }
        else {
            $data = HangHoa::create($request->all());
             //thêm hàng vào kho với số lượng là 0   
             DB::table('kho')->insert([
                "ID_HH" => $data['HH_ID'],
                "SOLUONG" => 0
             ]
                );
        }
        
        return Response::json($data);
        
    }
    public  function update(Request $request,$id)
    {
        
        $hh = HangHoa::find($id);

        
        $hh->HH_TEN = $request->HH_TEN;
        $hh->ID_DVT = $request->ID_DVT;
        $hh->ID_NH = $request->ID_NH;
        $hh->ID_QC = $request->ID_QC;
    
        $hh->save();
        
        return Response::json($hh);
    }
}

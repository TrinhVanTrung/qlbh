<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;

use Response;

use DB;

use Carbon\Carbon;

class NhanVienController extends Controller
{
    //
    
    function xemchitietdonhang(Request $request)
    {
        
        //load chi tiet don hang
        $data= DB::table('chitietdonhang')->where('ID_DH',$request->ID_DH);
        return Response::json($request->all());
    }
    
    function themchitietdonhang(Request $request)
    {
        //kiem tra ID_DH da  dc tao chua 
        // neu chua dc tao thi tao moi, neu da tao roi thi` add them khachhang
    $id_DH= DB::table('donhang')->where('DH_ID',$request->ID_DH)
                                    ->get();
    $id_NV = Auth::user()->id;
    if($request->id_NV!="")
    {
        $id_NV = $request->id_NV;
    }
        if(!isset($id_DH[0]->DH_ID))
        {
            //
                $date = Carbon::today();
                //$date->setTimezone('Asia/Ho_Chi_Minh');
            DB::table('donhang')->insert([
                'DH_ID' => $request->ID_DH,
                'ID_KH' => $request->ID_KH,
                'ID_NV' => $id_NV,
                'NGAYTAO' => $date,
                ]);
        }

        DB::table('chitietdonhang')->insert([
            'ID_DH'   => $request->ID_DH,
            'ID_HH'   => $request->ID_HH,
            'SOLUONG' => $request->SOLUONG,
            'DONGIA'  => $request->DONGIA,
            ]);
        
        $data= DB::table('chitietdonhang')->where('ID_DH',$request->ID_DH);
        return Response::json($request->all()); 
    }
    

     function capnhatchitietdonhang(Request $request,$id)
     {

        //neu  edit thi chuyen don hang e tarng thai chua duyet, vao chitietdonhang ve trang thai chua tru. 
        // Neu chitietdonhang da tru roi thi lay  soluong hien tai  + kho.  -> lay kho - soluong moi.
        $ctdh_status = DB::table('chitietdonhang')->where('CTDH_ID',$id)->get();
        if($ctdh_status[0]->CTDH_STATUS==1)
        {   
            //conglai vao kho soluong cu~ da tru 
            $slcu = DB::table('kho')->where('ID_HH',$ctdh_status[0]->ID_HH)->select('SOLUONG')->get();
            DB::table('kho')->where('ID_HH',$ctdh_status[0]->ID_HH)->update([
                    'SOLUONG'=> $slcu[0]->SOLUONG + $ctdh_status[0]->SOLUONG

                ]);
            // cap nhat lai trang thai chua tru kho
            DB::table('chitietdonhang')->where('CTDH_ID',$id)->update([
                    'SOLUONG'=> $request->SOLUONG,
                    'DONGIA' => $request->DONGIA,
                    'CTDH_STATUS' =>0
                ]);
            // cap nhat lai trang thai chua duyet
            DB::table('donhang')->where('DH_ID',$ctdh_status[0]->ID_DH)->update([
                    "DH_DUYET" => 0
                ]);

        }
        else
        {

            DB::table('chitietdonhang')->where('CTDH_ID',$id)->update([
                    'SOLUONG'=> $request->SOLUONG,
                    'DONGIA' => $request->DONGIA
                ]);
            
            
        }
        $data = ['update'=>'success'];
        return Response::json($data);
         
     }

     function xoadonhang(Request $request)
     {
        $ID_DH = $request->ID_DH.'-'.$request->ID_KH;
         
        DB::table('chitietdonhang')->where('ID_DH',$ID_DH)->delete();
        DB::table('donhang')->where('DH_ID',$ID_DH)->delete();
        $data =['delete'=>'ok'];
        return Response::json($data);
     }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Response;

use App\QuyCach;

class QuyCachController extends Controller
{
    //
    public function create(Request $request)
    {
        $id= QuyCach::find($request->get('QC_ID'));
        if($id)
        {
            $data= ['error'=>'idisset'];
        }
        else {
            $data = QuyCach::create($request->all());
        }
        
        return Response::json($data);
        
    }
    public  function update(Request $request,$id)
    {
        
        $quycach = QuyCach::find($id);

        $quycach->QC_ID = $request->QC_ID;
        $quycach->QC_TEN = $request->QC_TEN;
        $quycach->QC_GHICHU = $request->QC_GHICHU;
    
        $quycach->save();
    
        return Response::json($quycach);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Carbon\Carbon;

use Response;

use App\HangHoa;
use  App\Kho;


class KhoController extends Controller
{
    //
    public function create(Request $request)
    {
            $rs = $request->all(); // lay het du lieu post qua
             $id = DB::table('phieunhap')->where('PN_ID','=',$rs['PN_ID'])->get();
            if(count($id)<1)
            {
                $date = Carbon::now();
                $date->setTimezone('Asia/Ho_Chi_Minh');
                DB::table('phieunhap')->insert([
                "PN_ID" => $rs['PN_ID'],
                "NGAYNHAP"=>$date,
                "ID_NCC" =>"",
                             ]
                        ); 
            }
            // them vao chi tiet nhap kho
              DB::table('chitietnhapkho')->insert([
                  "ID_PN"=> $rs['PN_ID'],
                  "ID_HH"=> $rs['HH_ID'],
                  "SOLUONG" => $rs['SOLUONG'],
                  ]);  
            // tra du lieu chi tiet phieunhap
                $data = DB::table('chitietnhapkho')
                                ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietnhapkho.ID_HH')
                                ->leftJoin('nhomhang','nhomhang.NH_ID','=','hanghoa.ID_NH')
                                ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                                ->where('chitietnhapkho.ID_PN','=',$rs['PN_ID'])
                                ->get();
        return Response::json($data);
        
    }
    public  function update(Request $request,$id)
    {
        
        $hh =DB::table('chitietnhapkho')->where('ID_HH','=',$id)->where('ID_PN','=',$request->PN_ID)->update(['SOLUONG' =>$request->SOLUONG]);

        // tra du lieu chi tiet phieunhap
            $data = DB::table('chitietnhapkho')
                                ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietnhapkho.ID_HH')
                                ->leftJoin('nhomhang','nhomhang.NH_ID','=','hanghoa.ID_NH')
                                ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                                ->where('chitietnhapkho.ID_PN','=',$request['PN_ID'])
                                ->get();
        return Response::json($data);
    }
}

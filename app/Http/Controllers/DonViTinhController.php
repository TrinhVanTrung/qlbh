<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Response;

use App\DonViTinh;

class DonViTinhController extends Controller
{
    //
    public function create(Request $request)
    {
        $id= DonViTinh::find($request->get('DVT_ID'));
        if($id)
        {
            $data= ['error'=>'idisset'];
        }
        else {
            $data = DonViTinh::create($request->all());
        }
        
        return Response::json($data);
        
    }
    public  function update(Request $request,$id)
    {
        
        $dvt = DonViTinh::find($id);

        $dvt->DVT_ID = $request->DVT_ID;
        $dvt->DVT_TEN = $request->DVT_TEN;
        
        $dvt->save();
    
        return Response::json($dvt);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Response;

class KhachHangController extends Controller
{
    
    function create(Request $request)
    {
        $e =  DB::table('khachhang')->where('KH_ID',$request['KH_ID'])->get();
        $data= ['success'=>'ok'];   
        if(count($e)==1)
        {
            $data= ['error'=>'idisset']; 
        }
        else
        {
            DB::table('khachhang')->insert([
            'KH_TEN'=>$request['KH_TEN'],
            'KH_SDT'=>$request['KH_SDT'],
            'KH_DIACHI'=>$request['KH_DIACHI'],
            'ID_TD' => $request['ID_TD'],// id cua tuyen duong
            ]);
        }
        
        return Response::json($data);    
    }
    function update(Request $request,$id)
    {
        DB::table('khachhang')->where('KH_ID',$id)->update([
            'KH_TEN'=>$request['KH_TEN'],
            'KH_SDT'=>$request['KH_SDT'],
            'KH_DIACHI'=>$request['KH_DIACHI'],
            'ID_TD' => $request['ID_TD'],// id cua tuyen duong
            ]);
        $data = DB::table('khachhang')->get();    
        return Response::json($data);
    }
      
}

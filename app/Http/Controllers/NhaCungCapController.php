<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use DB;

use Response;

class NhaCungCapController extends Controller
{
    
    function create(Request $request)
    {
        $e =  DB::table('nhacungcap')->where('NCC_ID',$request['NCC_ID'])->get();
        $data= ['success'=>'ok'];   
        if(count($e)==1)
        {
            $data= ['error'=>'idisset']; 
        }
        else
        {
            DB::table('nhacungcap')->insert([
            'NCC_TEN'=>$request['NCC_TEN'],
            'NCC_SDT'=>$request['NCC_SDT'],
            'NCC_DIACHI'=>$request['NCC_DIACHI']
            ]);
        }
        
        return Response::json($data);    
    }
    function update(Request $request,$id)
    {
        DB::table('nhacungcap')->where('NCC_ID',$id)->update([
            'NCC_TEN'=>$request['NCC_TEN'],
            'NCC_SDT'=>$request['NCC_SDT'],
            'NCC_DIACHI'=>$request['NCC_DIACHI']
            ]);
        $data = DB::table('nhacungcap')->get();    
        return Response::json($data);
    }
      
}

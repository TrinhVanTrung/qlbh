<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Response;

use App\NhomHang;

class NhomHangController extends Controller
{
    public function create(Request $request)
    {
        $id= NhomHang::find($request->get('NH_ID'));
        if($id)
        {
            $data= ['error'=>'idisset'];
        }
        else {
            $data = NhomHang::create($request->all());
        }
        
        return Response::json($data);
        
    }
    public  function update(Request $request,$id)
    {
        
        $dvt = NhomHang::find($id);

        $dvt->NH_ID = $request->NH_ID;
        $dvt->NH_TEN = $request->NH_TEN;
        
        $dvt->save();
    
        return Response::json($dvt);
    }
}

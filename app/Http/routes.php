<?php

Route::get('/', function () {
    return view('login');
});

Route::get('/kho','AdminController@index')->middleware(['web','role:admin-kho']);

Route::get('/admin','AdminController@index')->middleware(['web','role:admin-kho']);

/**
 * 
 * QUY CACH 
 * group admin/quycach
 * 
 *
 **/

Route::group(['prefix'=>'/admin/quycach', 'middleware'=>['web','auth']],function (){
   
   //get index admin/quycach
   Route::get('/',function (){
     
        $data['data'] = App\QuyCach::all(); 
        return view('pages.quycach',$data);
    })->middleware('role:admin-kho');
    
    //resul table ajax , all 
    Route::get('/all',function(){
       
       $data['data']= App\QuyCach::all();
       return view('pages.ajax.tbquycach',$data)->renderSections()['content'];   
    })->middleware('role:admin-kho');
    //resul table ajax , all 
    Route::get('/allresponse',function(){
       
       $data= App\QuyCach::all();    
      return Response::json($data);
         
    });
    
    // get one record 
    Route::get('/{id?}',function($id){
        $data= App\QuyCach::find($id);
        return Response::json($data);
    });
    
    
    //post in create new record
    Route::post('/','QuyCachController@create')->middleware('role:admin-kho');
    
    // put update  record
    Route::put('/{id?}','QuyCachController@update')->middleware('role:admin-kho');
    
    // delete 1 record
    Route::delete('/{id?}',function($id){
    
        $data = App\QuyCach::destroy($id);
        return Response::json($data);
    })->middleware('role:admin-kho');
    
});

/**
 *  DON VI TINH
 * group admin/dvt
 *  
 *
 **/
Route::group(['prefix'=>'/admin/donvitinh','middleware'=>['web','auth']],function (){
   
   //get index admin/quycach
   Route::get('/',function (){
     
        $data['data'] = App\DonViTinh::all(); 
        return view('pages.donvitinh',$data);
    })->middleware('role:admin-kho');
    
    //resul table ajax , all 
    Route::get('/allresponse',function(){
       
       $data= App\DonViTinh::all();
    
      return Response::json($data);
    });
     //tra ve du lieu content cua donvitinh
    Route::get('/all',function(){
       
       $data['data']= App\DonViTinh::all();
    
      return view('pages.ajax.tbdonvitinh',$data)->renderSections()['content'];
    
         
    })->middleware('role:admin-kho');
    
    // get one record 
    Route::get('/{id?}',function($id){
        $data= App\DonViTinh::find($id);
        return Response::json($data);
    });
    
    //post in create new record
    Route::post('/','DonViTinhController@create')->middleware('role:admin')->middleware('role:admin-kho');
    
    // put update  record
    Route::put('/{id?}','DonViTinhController@update')->middleware('role:admin-kho');
    
    // delete 1 record
    Route::delete('/{id?}',function($id){
    
        $data = App\DonViTinh::destroy($id);
        return Response::json($data);
    })->middleware('role:admin-kho');
    
});


/**
 *  NHOM HANG
 * group admin/nhomhang
 *  
 *
 **/

Route::group(['prefix'=>'/admin/nhomhang','middleware'=>['web','auth']],function (){
   
   //get index admin/quycach
   Route::get('/',function (){
        $data['data'] = App\NhomHang::all(); 
        return view('pages.nhomhang',$data);
    })->middleware('role:admin-kho');
      //resul table ajax , all 
    Route::get('/allresponse',function(){
       
       $data= App\NhomHang::all();
    
      return Response::json($data);            
    });

    //resul table ajax , all 
    Route::get('/all',function(){
       
       $data['data']= App\NhomHang::all();
    
      return view('pages.ajax.tbnhomhang',$data)->renderSections()['content'];    
         
    })->middleware('role:admin-kho');

    // get one record 
    Route::get('/{id?}',function($id){
        $data= App\NhomHang::find($id);
        return Response::json($data);
    });
    
    //post in create new record
    Route::post('/','NhomHangController@create')->middleware('role:admin-kho');
    
    // put update  record
    Route::put('/{id?}','NhomHangController@update')->middleware('role:admin-kho');
    
    // delete 1 record
    Route::delete('/{id?}',function($id){
    
        $data = App\NhomHang::destroy($id);
        return Response::json($data);
    })->middleware('role:admin-kho');
    
});

/* 
**
    END GROUP NHOM HANG
**
*/

/**
 *  HANG HOA
 * group admin/hanghoa
 *  
 *
 **/
 Route::group(['prefix'=>'/admin/hanghoa','middleware'=>['web','auth']],function(){

     Route::get('/',function (){
         $data['data'] = DB::table('hanghoa')
                        ->leftJoin('donvitinh','DVT_ID','=','hanghoa.ID_DVT')
                        ->leftJoin('quycach','QC_ID','=','hanghoa.ID_QC')
                        ->leftJoin('nhomhang','NH_ID','=','hanghoa.ID_NH')
                        ->select('*')->get();
                        
         return view('pages.hanghoa',$data);
     })->middleware('role:admin-kho');
         //resul table ajax , all 
    Route::get('/all',function(){
       
       $data['data'] = DB::table('hanghoa')
                        ->leftJoin('donvitinh','DVT_ID','=','hanghoa.ID_DVT')
                        ->leftJoin('quycach','QC_ID','=','hanghoa.ID_QC')
                        ->leftJoin('nhomhang','NH_ID','=','hanghoa.ID_NH')
                        ->select('*')->get();
    
      return view('pages.ajax.tbhanghoa',$data)->renderSections()['content'];
    
         
    })->middleware('role:admin-kho');
    Route::get('/allresponse',function(){
       
       $data = DB::table('hanghoa')
                        ->leftJoin('donvitinh','DVT_ID','=','hanghoa.ID_DVT')
                        ->leftJoin('quycach','QC_ID','=','hanghoa.ID_QC')
                        ->leftJoin('nhomhang','NH_ID','=','hanghoa.ID_NH')
                        ->select('*')->get();
    
      return Response::json($data);
    
         
    });
      // get one record 
    Route::get('/{id?}',function($id){
        $data= App\HangHoa::find($id);
        return Response::json($data);
    });
    
     //post in create new record
    Route::post('/','HangHoaController@create')->middleware('role:admin-kho');
         // put update  record
    Route::put('/{id?}','HangHoaController@update')->middleware('role:admin-kho');
    
    // delete 1 record
    Route::delete('/{id?}',function($id){
    
        $data = App\HangHoa::destroy($id);
        return Response::json($data);
    })->middleware('role:admin-kho');
     
 });
 
 
 /**
 *  KHO
 * group admin/kho
 *  
 *
 **/
  Route::group(['prefix'=>'/admin/kho','middleware'=>['web','auth']],function(){
     Route::get('/',function (){
         
         $data['data'] = DB::table('hanghoa')
                            ->leftJoin('kho','ID_HH','=','HH_ID')
                            ->leftJoin('nhomhang','nhomhang.NH_ID','=','hanghoa.ID_NH')
                            ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                            ->get();
          
         return view('pages.kho',$data);
     })->middleware('role:admin-kho');

    Route::get('/nhapkho',function(){
        // xoa het luu tam
        DB::delete('DELETE FROM `chitietnhapkho` WHERE `ID_PN` = (SELECT PN_ID FROM `phieunhap`  WHERE SAVE_OFF=0)');
        DB::delete('DELETE FROM `phieunhap` WHERE `SAVE_OFF`=0');
        $data['data']=array();
        return view('pages.nhapkho',$data);
    })->middleware('role:admin-kho');
    //lichsunhapkho 
    Route::get('/lichsunhap/{id?}',function($id){
       
        $data = DB::table('chitietnhapkho')
                         ->leftJoin('hanghoa','chitietnhapkho.ID_HH','=','hanghoa.HH_ID')
                         ->leftJoin('phieunhap','phieunhap.PN_ID','=','chitietnhapkho.ID_PN')
                         ->where('hanghoa.HH_ID','=',$id)
                         ->select('chitietnhapkho.SOLUONG', 'phieunhap.NGAYNHAP','hanghoa.HH_TEN')
                         ->get();
                         
        return Response::json($data);
        
    });
         
    Route::get('/nhapkho/setsaveoff/{id?}',function($id){
       DB::table('phieunhap')
            ->where('PN_ID', $id)
            ->update(['SAVE_OFF' => 1]);
            
        //thuc hien luu vao kho
        ///1. lay toan bo chi tiet cua don nhap hang
        $chitietnhap = DB::table('chitietnhapkho')->where('ID_PN','=',$id)->get();
        foreach ($chitietnhap as $data) {
            $soluong= DB::table('kho')->where('ID_HH','=',$data->ID_HH)->select('SOLUONG')->get();
            
            DB::table('kho')->where('ID_HH',$data->ID_HH)->update(['SOLUONG'=> $soluong[0]->SOLUONG+$data->SOLUONG]);
        }
        $data = ['success'=>'ok'];
        return Response::json($data);
       
   });
      
    //resul table ajax , all 
    Route::get('/nhapkho/{id?}',function($id){
       
       $data['data'] = DB::table('chitietnhapkho')
                            ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietnhapkho.ID_HH')
                            ->leftJoin('nhomhang','nhomhang.NH_ID','=','hanghoa.ID_NH')
                            ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                            ->where('ID_PN','=',$id)
                            ->select('*')
                            ->get();
        $data['PN_ID'] =$id;
    
      return view('pages.ajax.tbnhapkho',$data)->renderSections()['content'];        
    })->middleware('role:admin-kho');
      
      // get one record 
    Route::get('/{id?}',function($id){
        $data= App\HangHoa::find($id);
        return Response::json($data);
    });
    
     //post in create new record
    Route::post('/nhapkho','KhoController@create')->middleware('role:admin-kho');
    
   // put update  record
    Route::put('/nhapkho/{id?}','KhoController@update')->middleware('role:admin-kho');
    
    // delete 1 record
    Route::delete('/nhapkho/{id?}/{pn_id?}',function($id,$pn_id){
    
        DB::table('chitietnhapkho')
                ->where('ID_HH','=',$id)
                ->where('ID_PN','=',$pn_id)
                ->delete();
        $data = ['success'=>'ok'];
            return Response::json($data);
    })->middleware('role:admin-kho');
     
 });

 /**
 *  NHANVIEN
 * group admin/nhanvien
 *  
 *
 **/
 Route::group(['prefix'=>'/admin/nhanvien','middleware'=>['web','auth']],function(){
     Route::get('/',function(){
         $data['data'] = DB::table('users')->where('role','user')->orwhere('role','kho')->get();
        return view('pages.nhanvien',$data);
     })->middleware('role:admin');
      
     
     Route::get('/all',function(){
            
            $data['data'] = DB::table('users')->where('role','user')->orwhere('role','kho')->get();
            return view('pages.ajax.tbnhanvien',$data)->renderSections()['content'];
     })->middleware('role:admin');
     Route::get('/{id?}',function($id){
         
         $data = DB::table('users')->where('id',$id)->get();
         return Response::json($data);
         
     });

     Route::post('/dangky','AdminController@create')->middleware('role:admin');
     Route::put('/{id?}','AdminController@update')->middleware('role:admin');
     Route::put('/changepass/{id?}','AdminController@changepass')->middleware('role:admin');
     
     Route::delete('/{id?}',function($id){
            DB::table('users')->where('id',$id)->delete();
            $data = DB::table('users')->where('role','user')->orwhere('role','kho')->get();
         return Response::json($data);
     })->middleware('role:admin');
 });
 

/** 
 * TUYEN DUONG
 * group admin/tuyenduong
 * 
 **/
 Route::group(['prefix'=>'/admin/tuyenduong','middleware'=>['web','auth']],function(){
     Route::get('/',function(){
         $data['data'] = DB::table('tuyenduong')->get();
        return view('pages.tuyenduong',$data);
     })->middleware('role:admin-kho');

     
     Route::get('/all',function(){
            
            $data['data'] = DB::table('tuyenduong')->get();
            return view('pages.ajax.tbtuyenduong',$data)->renderSections()['content'];
     });
     Route::get('/allresponse',function(){
            
            $data = DB::table('tuyenduong')->get();
            return Response::json($data);
     });
     Route::get('/{id?}',function($id){
         
         $data = DB::table('tuyenduong')->where('TD_ID',$id)->get();
         return Response::json($data);
         
     });
     Route::post('/','TuyenDuongController@create')->middleware('role:admin-kho');
     Route::put('/{id?}','TuyenDuongController@update')->middleware('role:admin-kho');
     
     Route::delete('/{id?}',function($id){
            DB::table('tuyenduong')->where('TD_ID',$id)->delete();
            $data = DB::table('tuyenduong')->get();
         return Response::json($data);
     })->middleware('role:admin-kho');
 });


/** 
 * NHA CUNG CAP
 * group admin/tuyenduong
 * 
 **/
 Route::group(['prefix'=>'/admin/nhacungcap'],function(){
     Route::get('/',function(){
         $data['data'] = DB::table('nhacungcap')->get();
        return view('pages.nhacungcap',$data);
     });
     Route::post('/','NhaCungCapController@create');
     Route::get('/all',function(){
            
            $data['data'] = DB::table('nhacungcap')->get();
            return view('pages.ajax.tbnhacungcap',$data)->renderSections()['content'];
     });
     Route::get('/{id?}',function($id){
         
         $data = DB::table('nhacungcap')->where('NCC_ID',$id)->get();
         return Response::json($data);
         
     });
     Route::put('/{id?}','NhaCungCapController@update');
     
     Route::delete('/{id?}',function($id){
            DB::table('nhacungcap')->where('NCC_ID',$id)->delete();
            $data = DB::table('nhacungcap')->get();
         return Response::json($data);
     });
 });
 /** 
 * KHACH HANG
 * group admin/khachhang
 * 
 **/
 Route::group(['prefix'=>'/admin/khachhang','middleware'=>['web','auth']],function(){
     Route::get('/',function(){
         $data['data'] = DB::table('khachhang')
                            ->leftJoin('tuyenduong','TD_ID','=','khachhang.ID_TD')   
                            ->get();
        return view('pages.khachhang',$data);
     })->middleware('role:admin-kho');
     
     Route::get('/allresponse/{id?}',function($id){
         
         $data  = DB::table('khachhang')->where('ID_TD',$id)->get();
         return Response::json($data);
     });
     
     Route::get('/all',function(){
            
            $data['data'] = DB::table('khachhang')
                            ->leftJoin('tuyenduong','TD_ID','=','khachhang.ID_TD')
                            ->get();
            return view('pages.ajax.tbkhachhang',$data)->renderSections()['content'];
     })->middleware('role:admin-kho');
     Route::get('/{id?}',function($id){
         
         $data = DB::table('khachhang')->where('KH_ID',$id)
                                        ->leftJoin('tuyenduong','TD_ID','=','khachhang.ID_TD')
                                        ->get();
         return Response::json($data);
         
     });
     Route::post('/','KhachHangController@create')->middleware('role:admin-kho');
     Route::put('/{id?}','KhachHangController@update')->middleware('role:admin-kho');

     Route::delete('/{id?}',function($id){
            DB::table('khachhang')->where('KH_ID',$id)->delete();
            $data = DB::table('khachhang')->get();
         return Response::json($data);
     })->middleware('role:admin-kho');
 });

/** 
 *   DONHANG
 *   group admin/donhang
 *
 **/
 Route::group(['prefix'=>'/admin/donhang','middleware'=>['web','auth']],function(){

    Route::get('/',function(){

        $date= Carbon\Carbon::today();
        $data['data'] = DB::table('donhang')
                        ->leftJoin('chitietdonhang','donhang.DH_ID','=','chitietdonhang.ID_DH')                        
                        ->leftJoin('users','users.id','=','donhang.ID_NV')
                        ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                        ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')
                        ->where('NGAYTAO',$date)
                        ->groupBy('users.id')                     
                        ->groupBy('khachhang.KH_ID')
                        ->get();
        $data['chuaduyet'] = DB::table('donhang')->where('DH_DUYET',0)->where('NGAYTAO','<',$date)->groupBy('NGAYTAO')->get();
        return view('pages.donhang',$data);
        
    })->middleware('role:admin-kho');
    /* cap nhat trang thai duyet */
    Route::PUT('/duyet/{id}',function(Request $request, $id){

            $DH_DUYET =  $request::get('DH_DUYET');
            DB::table('donhang')->where('DH_ID',$id)->update([
                   'DH_DUYET' => $DH_DUYET
                ]);
            //tru vao kho 
            $chitiet = DB::table('chitietdonhang')->where('ID_DH',$id)->where('CTDH_STATUS',0)->get(); // lay  nhung chitietdonhang voi trang thai chua tru vao kho de tru.
            foreach ($chitiet as $key => $value) {
                $sl = DB::table('kho')->where('ID_HH',$value->ID_HH)->get();
                $slmoi = $sl[0]->SOLUONG - $value->SOLUONG;
                DB::table('kho')->where('ID_HH',$value->ID_HH)->update([
                        'SOLUONG' =>   $slmoi
                    ]);
                DB::table('chitietdonhang')->where('CTDH_ID',$value->CTDH_ID)->update([
                    'CTDH_STATUS'=> 1
                ]);
            }
        $data = ['update'=>'success'];
        return Response::json($data);

    });
    // load  chitietdonhang theo ma don hang
    Route::get('/load/donhang/{id}',function($id){

            $data = DB::table('chitietdonhang')
                                ->leftJoin('donhang','donhang.DH_ID','=','chitietdonhang.ID_DH')
                                ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietdonhang.ID_HH')
                                ->leftJoin('kho','hanghoa.HH_ID','=','kho.ID_HH')
                                ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                                ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')  
                                ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                                ->where('ID_DH',$id)
                                ->select('*','kho.SOLUONG as SLKHO', 'chitietdonhang.SOLUONG as SOLUONG')
                                ->get();
            return Response::json($data);

    });
    //load 1 chitietdonhang
    Route::get('/load/chitietdonhang/{id}',function($id){

            $data = DB::table('chitietdonhang')
                                ->leftJoin('donhang','donhang.DH_ID','=','chitietdonhang.ID_DH')
                                ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietdonhang.ID_HH')
                                ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                                ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')  
                                ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                                ->where('CTDH_ID',$id)
                                ->get();
            return Response::json($data);

    });
    // xoa  1 chi tiet don hang
    Route::delete('/chitietdonhang/{id?}',function($id){
        $ctdh_status = DB::table('chitietdonhang')->where('CTDH_ID',$id)->get();
        if($ctdh_status[0]->CTDH_STATUS==1)
        {   
            //conglai vao kho soluong cu~ da tru 
            $slcu = DB::table('kho')->where('ID_HH',$ctdh_status[0]->ID_HH)->select('SOLUONG')->get();
            DB::table('kho')->where('ID_HH',$ctdh_status[0]->ID_HH)->update([
                    'SOLUONG'=> $slcu[0]->SOLUONG + $ctdh_status[0]->SOLUONG
                ]);
        }
        DB::table('chitietdonhang')->where('CTDH_ID',$id)->delete();
        $data = ['delete'=>'success'];
        return Response::json($data);
    })->middleware('role:admin-kho');
    // update chitietdonhang
    Route::put('/chitietdonhang/{id}','NhanVienController@capnhatchitietdonhang')->middleware('role:admin-kho');
    // load user voi role la user
    Route::get('/users',function(){
        $data = DB::table('users')->where('role','user')->get();
        return Response::json($data);
    });

    //load don hang  theo id user va ngay thang
    Route::get('/user/{id}/{date}',function($id,$date){
            
        $str = explode('-',$date);        
        $date = $str[2].'-'.$str[1].'-'.$str[0];
        $data ="";
        if($id=='all') //truong hop cmbNhanvien khong chon, load all
        {    
        $data = DB::table('donhang')
                        ->leftJoin('chitietdonhang','donhang.DH_ID','=','chitietdonhang.ID_DH')                        
                        ->leftJoin('users','users.id','=','donhang.ID_NV')
                        ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                        ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')
                        ->where('NGAYTAO',$date)                        
                        ->groupBy('users.id')   
                        ->groupBy('khachhang.KH_ID')                  
                        ->get();
        }
        else
        {
            $data = DB::table('donhang')
                        ->leftJoin('chitietdonhang','donhang.DH_ID','=','chitietdonhang.ID_DH')                        
                        ->leftJoin('users','users.id','=','donhang.ID_NV')
                        ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                        ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')
                        ->where('NGAYTAO',$date)
                        ->where('ID_NV',$id)
                        ->groupBy('users.id') 
                        ->groupBy('khachhang.KH_ID')                                      
                        ->get();


        }

        return Response::json($data);
    });
    //xoa 1 don hang ->xoat het chi tiet don hang 
    Route::delete('/xoadonhang/{id}',function($id){

        $ID_DH = $id;
        $dh_duyet=DB::table('donhang')->where('DH_ID',$id)->select('DH_DUYET')->get();        
        if($dh_duyet[0]->DH_DUYET==1) 
        {
            //tra hang ve kho ne 
            // toi can lay chitietdonhang theo ma don hang 
            $chitiet = DB::table('chitietdonhang')->where('ID_DH',$id)->get();
            //toi se duyet qua tung  chitiet lay ra chitiet da duyet tra so luong do ve kho
            foreach ($chitiet as $key => $value) {
                $slcu = DB::table('kho')->where('ID_HH',$value->ID_HH)->get();
                DB::table('kho')->where('ID_HH',$value->ID_HH)->update([
                    'SOLUONG' => $slcu[0]->SOLUONG + $value->SOLUONG
                    ]); 
              }            
        
        }
        
        DB::table('chitietdonhang')->where('ID_DH',$ID_DH)->delete();
        DB::table('donhang')->where('DH_ID',$ID_DH)->delete();
        $data =['delete'=>'ok'];    
        return Response::json($data);
    })->middleware('role:admin-kho');

 });


/** 
*  BANHANG
*  group admin/banhang
**/
Route::group(['prefix'=>'/admin/banhang','middleware'=>['web','auth','role:admin']],function(){

        Route::get('/',function(){
        
        $data['data'] = array(); 
        return view('pages.admin-banhang',$data);

        });
        Route::get('/updatev/{id}',function($id){

            $data = DB::table('chitietdonhang')
                                        ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietdonhang.ID_HH')
                                        ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                                            ->where('ID_DH',$id)->get();
            return Response::json($data);
        });
        
       
});

/**
*   TRAHANG
*   Group /admin/trahang
**/  
Route::group(['prefix'=>'/admin/trahang','middleware'=>['web','auth','role:admin']],function(){

    Route::get('/',function(){
        $data['data'] = array();
        return view('pages.trahang',$data);
    });

    Route::post('/',function(Request $request){

            DB::table('trahang')->insert([
                'ID_DH'     =>$request::get('ID_DH'),
                'ID_HH'     =>$request::get('ID_HH'),
                'SOLUONG'   =>$request::get('SOLUONG'),
                'DONGIA'    =>$request::get('DONGIA'),
                'GHICHU'    =>$request::get('GHICHU'),
                ]);
               //cong hang lai kho 
              $q = DB::table('kho')->where('ID_HH',$request::get('ID_HH'))->get();
              DB::table('kho')->where('ID_HH',$request::get('ID_HH'))->update([
                'SOLUONG' => $q[0]->SOLUONG + $request::get('SOLUONG')
                ]);
            $data = ['post'=>'success'];
            return Response::json($data);

    });

    Route::get('/{id}',function($id){

         $data =  DB::table('trahang')
                            ->leftJoin('donhang','donhang.DH_ID','=','trahang.ID_DH')
                            ->leftJoin('hanghoa','hanghoa.HH_ID','=','trahang.ID_HH')
                            ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                            ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')                            
                            ->where('trahang.ID_DH','=' ,$id)
                            ->get();
         
         return Response::json($data);

    });

    Route::delete('/{id}',function($id){

                $qa = DB::table('trahang')->where('TH_ID',$id)->get();

                $q = DB::table('kho')->where('ID_HH',$qa[0]->ID_HH)->get();
              
              DB::table('kho')->where('ID_HH',$qa[0]->ID_HH)->update([
                'SOLUONG' => $q[0]->SOLUONG - $qa[0]->SOLUONG
                ]);
              DB::table('trahang')->where('TH_ID',$id)->delete();

              $data = ['delete'=>'success'];

              return Response::json($data);

    });

    //load 1 don hang theo ma don hang 
    Route::get('/load/donhang/{id}',function($id){

            $data = DB::table('donhang')->where('DH_ID',$id)->get();
            return Response::json($data);
    });

});

/** 
*  DOANHTHU
*
**/
Route::group(['prefix'=>'/admin/doanhthu','middleware'=>['web','auth']], function (){

    Route::get('/',function(){

        $data['data'] =  array();
        return view('pages.doanhthu',$data);
    });

    Route::post('/user/{id}/',function(Request $request,$id){

        $day = $request::get('day');
        $arr_day = explode('-', $day);
        $tungay = explode('/', $arr_day[0]);
        $denngay = explode('/', $arr_day[1]);
        $d_tungay = trim($tungay[2]).'-'.trim($tungay[1]).'-'.trim($tungay[0]);
        $d_denngay = trim($denngay[2]).'-'.trim($denngay[1]).'-'.trim($denngay[0]);
        $data['theohoadon'] = DB::table('donhang')
                    ->leftJoin('chitietdonhang','chitietdonhang.ID_DH','=','donhang.DH_ID')
                    ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')  
                    ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')
                    ->leftJoin('users','users.id','=','donhang.ID_NV')                    
                    ->where('ID_NV',$id)
                    ->where('NGAYTAO','>=',$d_tungay)
                    ->where('NGAYTAO','<=',$d_denngay)
                    ->where('CTDH_STATUS',1)
                    ->select(DB::raw("SUM(chitietdonhang.DONGIA*chitietdonhang.SOLUONG) AS THANHTIEN, DH_ID, KH_TEN, TD_TEN, name"))                
                    //->groupBy('DH_ID')
                    ->groupBy('KH_ID') 
                    ->get();
        
        $data['trahang'] = DB::table('trahang')
                                ->leftJoin('donhang','trahang.ID_DH','=','donhang.DH_ID')
                                ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')                                
                                ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')
                                ->leftJoin('users','users.id','=','donhang.ID_NV')                                  
                                ->where('donhang.ID_NV',$id)
                                ->where('donhang.NGAYTAO','>=',$d_tungay)
                                ->where('donhang.NGAYTAO','<=',$d_denngay)
                                ->select(DB::raw("SUM(SOLUONG*DONGIA) AS TIENTRA, KH_TEN, TD_TEN, name")) 
                                //->groupBy('DH_ID')                               
                                ->groupBy('KH_ID')
                                ->get();
        return Response::json($data);
    });

});




/* ************************************************************************** */

/**
 * #NHANVIEN 
 * 
 * Group /nhanvien
 * 
 **/
Route::group(['prefix'=>'/nhanvien', 'middleware'=>['web','auth']],function(){
   //giao dien nhan vien
   Route::get('/',function(){
    //cont hieu id nhan vien
        $data['data']=DB::table('donhang')
                        ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                        ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')                        
                        ->where('NGAYTAO',Carbon\Carbon::today())                        
                        ->where('ID_NV',Auth::user()->id)->get();

        return view('pages.nhanvien-nv',$data);
   });
   // giao dien ban hang 
   Route::get('/banhang',function(){
            
            $data['data'] = array(); //load chitietdonhang theo ma don hang
            return view('pages.banhang',$data);
     });

Route::get('/doanhthu',function() {
        $data['data'] =  array();
        return view('pages.nv-doanhthu',$data);
       
   });

   Route::get('/hanghoa',function(){

        $data['data'] = DB::table('hanghoa')
                        ->leftJoin('donvitinh','DVT_ID','=','hanghoa.ID_DVT')
                        ->leftJoin('quycach','QC_ID','=','hanghoa.ID_QC')
                        ->leftJoin('nhomhang','NH_ID','=','hanghoa.ID_NH')
                        ->select('*')->get();
                        
         return view('pages.hanghoa-nv',$data);

   });

   Route::get('/khachhang',function(){

        $data['data'] = DB::table('khachhang')
                        ->leftJoin('tuyenduong','TD_ID','=','ID_TD')                       
                        ->select('*')->get();
                        
         return view('pages.khachhang-nv',$data);

   });
   Route::get('/tuyenduong',function(){

        $data['data'] = DB::table('tuyenduong')                 
                        ->select('*')->get();
                        
         return view('pages.tuyenduong-nv',$data);

   });
   Route::get('/banhang/{id}',function($id){
            
            $date =Carbon\Carbon::today();            
            $data['data']= array();
               $id_DH = 'DH'.$date->year.$date->month.$date->day; 
            $e = explode('-',$id);            
            if($id_DH==$e[0])
            {
                
                $data['data'] = DB::table('chitietdonhang')
                                ->leftJoin('donhang','donhang.DH_ID','=','chitietdonhang.ID_DH')
                                ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietdonhang.ID_HH')
                                ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                                ->leftJoin('tuyenduong','tuyenduong.TD_ID','=','khachhang.ID_TD')  
                                ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                                ->where('ID_DH',$id)
                                ->get(); 
            return view('pages.banhang',$data);
            }
            return redirect('/nhanvien/banhang');
            

            
     });

    // cap nhat xem o trang giao dien ban hang
    Route::get('/updatev/{id?}',function($id){            
            $data['data'] = DB::table('chitietdonhang')
                                        ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietdonhang.ID_HH')
                                        ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                                        ->where('ID_DH',$id)->get();
            //load chitietdonhang theo ma don hang-
            return view('pages.ajax.tbbanhang',$data)->renderSections()['content'];
     });
    
     
     //tra ve 1 id cua chi tiet don  hang 
    Route::get('/chitietdonhang/{id?}',function($id){
        
        $data = DB::table('chitietdonhang')->where('CTDH_ID',$id)
                                           ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietdonhang.ID_HH')
                                           ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                                           ->get();
        return Response::json($data);
        
    });
    // xoa  1 chi tiet don hang
    Route::delete('/chitietdonhang/{id?}',function($id){
        
        DB::table('chitietdonhang')->where('CTDH_ID',$id)->delete();
        $data = ['delete'=>'success'];
        return Response::json($data);
    });

    //xoa  don hang cua khach hang, xoa toan bo  chitietdonhang cua khach hang do
    Route::post('/xoadonhang','NhanVienController@xoadonhang');

    //cap nhat chi chitietdon hang
    Route::put('/chitietdonhang/{id?}','NhanVienController@capnhatchitietdonhang');
    
    //them chi tiet don hang
    Route::post('/themchitietdonhang','NhanVienController@themchitietdonhang');
     
    // ??? 
    Route::post('/chitietdonhang','NhanVienController@xemchitietdonhang');   
    
});



/*
|--------------------------------------------------------------------------
|  Route Print
|--------------------------------------------------------------------------
|
| Nhan du lieu vao  hien trang in.
|
*/
Route::group(['prefix'=>'in'],function(){

    Route::get('/donhanga5/{day}/{idnv}',function($day,$idnv){
             $d = explode('-', $day);
             $day = $d[2].'-'.$d[1].'-'.$d[0];
            $data['data'] = DB::table('donhang')                            
                            ->leftJoin('users','users.id','=','donhang.ID_NV')
                            ->leftJoin('khachhang','khachhang.KH_ID','=','donhang.ID_KH')
                            ->where('users.id',$idnv)
                            ->where('donhang.NGAYTAO',$day)                            
                            ->where('donhang.DH_DUYET',1)
                            ->select('users.id','donhang.*','khachhang.*')                            
                            ->get();

            foreach ($data['data'] as $key => $value) {
                        $data['chitiet'][$value->DH_ID] = DB::table('chitietdonhang')
                                                        ->where('ID_DH',$value->DH_ID)
                                                        ->leftJoin('hanghoa','hanghoa.HH_ID','=','chitietdonhang.ID_HH')
                                                        ->leftJoin('quycach','quycach.QC_ID','=','hanghoa.ID_QC')
                                                        ->leftJoin('donvitinh','donvitinh.DVT_ID','=','hanghoa.ID_DVT')
                                                        ->get();

                            }                
          return view('pages.printdonhangA5',$data);
    });
});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => ['web']], function () {
Route::get('/', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);    
Route::get('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@showLoginForm']);    

Route::post('login', ['as' => 'auth.login', 'uses' => 'Auth\AuthController@login']);
Route::get('logout', ['as' => 'auth.logout', 'uses' => 'Auth\AuthController@logout']);


// Password Reset Routes...
//Route::get('password/reset/{token?}', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@showResetForm']);
//Route::post('password/email', ['as' => 'auth.password.email', 'uses' => 'Auth\PasswordController@sendResetLinkEmail']);
//Route::post('password/reset', ['as' => 'auth.password.reset', 'uses' => 'Auth\PasswordController@reset']);
  
// Registration Routes...
// $this->get('register', 'Auth\AuthController@showRegistrationForm');
// $this->post('register', 'Auth\AuthController@register');

});


Route::get('/home', 'HomeController@index');

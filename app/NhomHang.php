<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NhomHang extends Model
{
    //
    protected $table="nhomhang";
    protected $fillable=['NH_ID','NH_TEN'];
    public $primaryKey='NH_ID';
    public $incrementing = false;
    
   
}

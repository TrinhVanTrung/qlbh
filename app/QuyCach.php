<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuyCach extends Model
{
    //
    protected $table='quycach';
    protected $fillable=['QC_ID','QC_TEN','QC_GHICHU'];
    public $primaryKey ='QC_ID';
    public $incrementing = false;
}

$(document).ready(function(){

    var  url ="/public/nhanvien"; // dinh huong url  cho ajax
    var name ='BÁN HÀNG';
    var  idmodal = '#banhangModal';
    var  idfrm = '#frmBanHang';
    var  id_ma = '#chitietdonhang_id';
    var  id_TD =  '';

    if($('#today').val()=="") 
    {   

        //add datacombo        
        addDataForSelect('#cmbTuyenduong','/public/admin/tuyenduong/allresponse','TD_ID','TD_TEN','','');        

    }
    else
    {        
            var dt = JSON.parse($('#today').val());
            //add datacombo
            addDataForSelect('#cmbTuyenduong','/public/admin/tuyenduong/allresponse','TD_ID','TD_TEN',dt.ID_TD,'');
            addDataForSelect('#cmbKhachhang','/public/admin/khachhang/allresponse/'+dt.ID_TD,'KH_ID','KH_TEN',dt.ID_KH,'');
            setautoNumeric('.tiente','tiente');
        $('.tiente').autoNumeric('set',$('.tiente').html()); 
    }
    var  id_DH = taoIdDonhang('DH');
    
    showDay('#today');
    setautoNumeric('#txtSoluong','number');
    setautoNumeric('#txtDongia','tiente');  
    addDataForSelect('#Idnhanvien','/public/admin/donhang/users','id','name','','');
    addDataForSelect('#cmbHanghoa','/public/admin/hanghoa/allresponse','HH_ID','HH_TEN','','QC_TEN');
    $(document).on('change','#cmbTuyenduong',function(){
            id_TD = $('#cmbTuyenduong').val();
            addDataForSelect('#cmbKhachhang','/public/admin/khachhang/allresponse/'+id_TD,'KH_ID','KH_TEN','','');
        });
    
    $(document).on('change','#cmbKhachhang',function(){
        
        id_KH = $('#cmbKhachhang').val();
        id_TD = $('#cmbTuyenduong').val();
        id_NV = $('#Idnhanvien').val();

      var  frmdata = {
            ID_KH: id_KH,
            ID_DH:  id_DH,
            ID_NV: id_NV
        };
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        })
        $.ajax({
          url: url+'/chitietdonhang' ,
          type: 'post',
          dataType: 'json',
          data: frmdata,
          success: function (data){
              //console.log(data);
               var my_url = '/public/admin/banhang/updatev/'+id_DH+'-'+id_KH;
                $.get(my_url,function(data){
                       //console.log(data);
                var db_select="";
                var name = "Không có dữ liệu";
                   for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                db_select+= '<tr>';                        
                                db_select+= "<td>" + data[key]['HH_TEN']+ data[key]['QC_TEN'] + "</td>";
                                db_select+= '<td>' + data[key]['SOLUONG'] + "</td>";
                                db_select+= '<td>' + data[key]['DONGIA']  + "</td>";                                                
                                db_select+= '<td>' + ' <button class="btn open-modal" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-pencil text-primary"></i> </button><button class="btn delete" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-close text-danger "></i> </button>'
                                          + '</td>';
                                db_select+= '</tr>';
                            }
                          }//endfor 
                          $('#datatable tbody').html(db_select);                  
               });
          },
          error: function(data){
              
          }
            
        });
    });
    
    //  load dữ liệu vào modal form này.
    $(document).on('click','.open-modal',function(e){
       e.preventDefault();
        var id = $(this).val(); // id nằm trong btn có class .open-mal
        $.get(url + '/chitietdonhang/' + id, function (data) {
            console.log(data[0].DONGIA);
            $(id_ma).val(data[0].CTDH_ID); // ma chitietdonhang
            $('#txtTenhang').val(data[0].HH_TEN+' - '+data[0].QC_TEN);
            $('#txtSoluongedit').val(data[0].SOLUONG);            
            setautoNumeric('#txtDongiaedit','tiente');
            $('#txtDongiaedit').autoNumeric('set',data[0].DONGIA);
            $('#btn-save').val("update"); // chuyển value về trạng thái  'update'            
        }) ;
        $(idmodal).modal();
    });
    
    //display modal form for creating new task , ham them hang  vao chi tiet hang hoa
    $(document).on('click','#btnAdd',function(){
        
        if( $('#cmbTuyenduong').val()=="" || $('#cmbKhachhang').val()=="" || $('#cmbHanghoa').val()=="" || $('#txtSoluong').val()=="" || $('#txtDongia').autoNumeric('get')=="" || $('#Idnhanvien').val()=="")
        {
            
            show_stack_bottomright('error');
            return false;    
        }
        var id_KH = $('#cmbKhachhang').val();
        var id_TD = $('#cmbTuyenduong').val();
        var frmdata = {
            ID_HH: $('#cmbHanghoa').val() ,
            ID_DH: id_DH+'-'+id_KH,
            ID_KH: id_KH,
            SOLUONG: $('#txtSoluong').val(),
            DONGIA: $('#txtDongia').autoNumeric('get'),
            id_NV: $('#Idnhanvien').val(),
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        })
        $.ajax({
           url: url+'/themchitietdonhang',
           type: 'post',
           dataType: 'json',
           data: frmdata,
           success:function(data){
                var my_url = '/public/admin/banhang/updatev/'+id_DH+'-'+id_KH;
                $.get(my_url,function(data){
                       //console.log(data);
                var db_select="";
                var name = "Không có dữ liệu";
                   for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                db_select+= '<tr>';                        
                                db_select+= "<td>" + data[key]['HH_TEN']+ data[key]['QC_TEN'] + "</td>";
                                db_select+= '<td>' + data[key]['SOLUONG'] + "</td>";
                                db_select+= '<td>' + data[key]['DONGIA']  + "</td>";                                                
                                db_select+= '<td>' + ' <button class="btn open-modal" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-pencil text-primary"></i> </button><button class="btn delete" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-close text-danger "></i> </button>'
                                          + '</td>';
                                db_select+= '</tr>';
                            }
                          }//endfor 
                          $('#datatable tbody').html(db_select);                  
               });                
               $('#frmthemhanghoa').trigger('reset');
               $("#cmbHanghoa").select2("val",' ');
               
           },
           error: function(data){
               console.log(data);
           }
           
            
        });
    });
    

    //delete task and remove it from list
    //****************** XOA RECORD *************************
    
    $(document).on('click','.delete',function(e){
        var id = $(this).val();
        var id_KH = $('#cmbKhachhang').val();
                                var id_TD = $('#cmbTuyenduong').val();
            $('#delModal').on('show.bs.modal', function(){
             $('#delModal').find('button.btnXoa').attr('id','btnxoa'+id);
             $('#delModal').find('.modal-title').text('THÔNG BÁO');
             $('#delModal').find('.modal-body').text('Bạn có thật sự muốn xóa');
           });
            $('#delModal').modal('show');
            $(document).on('click','#btnxoa'+id,function(e) {
                e.preventDefault();
                 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                });
                $.ajax({
                    type: "DELETE",
                    url: url + '/chitietdonhang/' +id,
                    success: function (data) {
                      //  console.log(data);
                       $('#delModal').on('hidden.bs.modal',function(){
                            
                            var my_url = '/public/admin/banhang/updatev/'+id_DH+'-'+id_KH;
                                $.get(my_url,function(data){
                                       //console.log(data);
                                var db_select="";
                                var name = "Không có dữ liệu";
                                   for (var key in data) {
                                        if (data.hasOwnProperty(key)) {
                                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                                db_select+= '<tr>';                        
                                                db_select+= "<td>" + data[key]['HH_TEN']+ data[key]['QC_TEN'] + "</td>";
                                                db_select+= '<td>' + data[key]['SOLUONG'] + "</td>";
                                                db_select+= '<td>' + data[key]['DONGIA']  + "</td>";                                                
                                                db_select+= '<td>' + ' <button class="btn open-modal" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-pencil text-primary"></i> </button><button class="btn delete" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-close text-danger "></i> </button>'
                                                          + '</td>';
                                                db_select+= '</tr>';
                                            }
                                          }//endfor 
                                          $('#datatable tbody').html(db_select);                  
                               });                
                               $('#frmthemhanghoa').trigger('reset');
                               $("#cmbHanghoa").select2("val",' ');

                            });
                    
                     $("#delModal").modal("hide");
                    },
                    error: function (data) {
                        //console.log('Error:', data);
                    }
                });
             });
            
    });
        
    //******************END  XOA RECORD *********************

    //create new task / update existing task
    //****************** THÊM VÀ CẬP NHẬT RECORD *************************
    $(document).on('click','#btn-save',function(e) {
        
        e.preventDefault();
        // bay loi nhap vao 
       if( $('#txtTenhang').val()=="" || $('#txtSoluongedit').val()=="" || $('#txtDongiaedit').val()=="" )
        {
            var str_alert = "<strong>Kiểm tra lại!</strong> Không được bỏ trống.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
                    
            return false;
        }
        var alphaExp = /^[0-9a-zA-Z,.]+$/;
        // lay du lieu tu formvao , luu Y: Truong nhan du lieu vao la truong giong voi field trong table
        // trim(): xoa khoan trang du thua.
        var formData = {
            DONGIA: $('#txtDongiaedit').autoNumeric('get'),
            SOLUONG: $('#txtSoluongedit').val().trim(),
            
        }
        
        //used to determine the http verb to use [add=POST], [update=PUT]
        // kiểm tra thêm mới hay cập nhật
        var state = $('#btn-save').val(); // trong nút có attribute có value

        var type = 'POST'; //for creating new resource
        var id = $(id_ma).val(); // trong nút có id là id của record,
        var my_url = url;
        var id_KH = $('#cmbKhachhang').val();
        var id_TD =  $('#cmbTuyenduong').val(); 

        if (state == 'update'){
            type = 'PUT'; //for updating existing resource
            my_url = url + '/chitietdonhang/' + id;
        }
        //console.log(formData);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        }); 
        $.ajax({
            type: type,
            url: my_url,            
            data: formData,           
            dataType: 'json',            
            success: function (data) {
                //console.log(data);
                if(data.error=='idisset') // trường hợp trùng id
                {   
                    var str_alert = "<strong>Có rồi!</strong> Mã vừa nhập đã có  rồi hãy nhập mà mới.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
                    
                }
                else
                {
               
                    $(idmodal).on('hidden.bs.modal',function(){
                    
                        var my_url = '/public/admin/banhang/updatev/'+id_DH+'-'+id_KH;
                            $.get(my_url,function(data){
                                   //console.log(data);
                            var db_select="";
                            var name = "Không có dữ liệu";
                               for (var key in data) {
                                    if (data.hasOwnProperty(key)) {
                                      //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                            db_select+= '<tr>';                        
                                            db_select+= "<td>" + data[key]['HH_TEN']+ data[key]['QC_TEN'] + "</td>";
                                            db_select+= '<td>' + data[key]['SOLUONG'] + "</td>";
                                            db_select+= '<td>' + data[key]['DONGIA']  + "</td>";                                                
                                            db_select+= '<td>' + ' <button class="btn open-modal" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-pencil text-primary"></i> </button><button class="btn delete" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-close text-danger "></i> </button>'
                                                      + '</td>';
                                            db_select+= '</tr>';
                                        }
                                      }//endfor 
                                      $('#datatable tbody').html(db_select);                  
                           });                
                           $('#frmthemhanghoa').trigger('reset');
                           $("#cmbHanghoa").select2("val",' '); 



                    });
                    
                    $(idmodal).modal('hide');
                }// end kiểm tra tồn tại id  và thêm mới 
            }, // end   ajax  post xử lý dữ liệu xong
            error: function (data) {
                //console.log('Error:', data); // thông báo lỗi qua console 
            }
        });
    });
    //******************END THÊM VÀ CẬP NHẬT RECORD *************************
    
    
    $(document).on('click','#btn-huy',function(){
                   (new PNotify({
                        title: 'Xác nhận Hủy đơn hàng',
                        text: 'Bạn  muốn hủy không?',
                        icon: 'glyphicon glyphicon-question-sign',
                        styling: 'bootstrap3',
                        hide: false,
                        confirm: {
                            confirm: true
                        },
                        buttons: {
                            closer: false,
                            sticker: false
                        },
                        history: {
                            history: false
                        }
                    })).get().on('pnotify.confirm', function() {
                        //  click ok se huy 
                        var id_KH =  $('#cmbKhachhang').val();
                        var data = {
                            ID_DH : id_DH,
                            ID_KH : id_KH
                        }
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
                            }
                        });
                        $.ajax({
                            url: '/nhanvien/xoadonhang',
                            type: 'post',
                            data: data,
                            dataType: 'json',
                            success:function(data)
                            {
                                if(data.delete=="ok")
                                {
                                      window.location.href='/nhanvien/banhang';
                                }
                            },
                            error: function(data)
                            {},

                        });
                        
                    }).on('pnotify.cancel', function() {
                        
                    });
        
    }); //end functin  btn-huy
});


function reloadAll(id_KH,id_TD)
{
        showDay('#today'); // hien ngay thang 
        // set datatalbe cho bang bieu, bo di thuoc tinh xem 
        $('#datatable').DataTable({
                "language": {
                    "url": '/public/Vietnamese.json',
                },
                responsive: true,
                "sDom": 'f,t,i',                                    
            });
         // du lieu cho combo   
        addDataForSelect('#Idnhanvien','/public/admin/donhang/users','id','name','','');
        addDataForSelect('#cmbTuyenduong','/public/admin/tuyenduong/allresponse','TD_ID','TD_TEN',id_TD,'');
        addDataForSelect('#cmbTuyenduong','/public/admin/tuyenduong/allresponse','TD_ID','TD_TEN',id_TD,'');
        addDataForSelect('#cmbKhachhang','/public/admin/khachhang/allresponse/'+id_TD,'KH_ID','KH_TEN',id_KH,'');
        addDataForSelect('#cmbHanghoa','/public/admin/hanghoa/allresponse','HH_ID','HH_TEN','','QC_TEN');
        setautoNumeric('#txtSoluong','number');
        setautoNumeric('#txtDongia','tiente');
        setautoNumeric('.tiente','tiente');
        $('.tiente').autoNumeric('set',$('.tiente').html()); // say oh yeah
}
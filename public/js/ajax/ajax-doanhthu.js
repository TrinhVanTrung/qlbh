$(document).ready(function(){

	var url = '/public/admin/doanhthu';
  var  day ="";
  var  id_NV =$('#id_nv').val();
  if(id_NV=="")
  {
    addDataForSelect('#cmbNV','/public/admin/donhang/users','id','name','','');  
    $('#cmbNV').prop("disabled", false);
  }
  else
  {
      addDataForSelect('#cmbNV','/public/admin/donhang/users','id','name',id_NV,'');  
      $('#cmbNV').prop("disabled", true);
      
  }
  
//	addDataForSelect('#cmbNV','/public/admin/donhang/users','id','name','','');
	//setDataTable('#datatable');
	//hien ngay thang
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    var h = dateObj.getHours();
    var m = dateObj.getMinutes();
    var s = dateObj.getSeconds();
    newid =day + '/' +month + '/' + year ;
   
	 $('#single_cal2').daterangepicker({
          //singleDatePicker: false,
          //calender_style: "picker_2", 
         // autoUpdateInput: false, 
          "autoApply": true,
          "showDropdowns": true,
          locale: {
           "format": 'DD/MM/YYYY',
           "cancelLabel": 'Clear',
           "fromLabel": "Từ ngày",
           "toLabel": "đền ngày",
          } ,

        }, function(start, end, label) {
          //console.log(start.toISOString(), end.toISOString(), label,start.format('DD/MM/YYYY'));
        });
	 $('#single_cal2').val(newid+ ' - ' + newid );
	  $('#single_cal2').on('apply.daterangepicker', function(ev, picker) {
       $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
      
  });

    /*  fill don hang theo nhan vien */
	$(document).on('click','#btn-fill',function(){

		id_NV = $('#cmbNV').val();
    if(id_NV=='') {id_NV='all'}
     day =  $('#single_cal2').val();

		var my_url = url + '/user/'+id_NV;
    var formData = {
      "day" : day
    }
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        });
    $.ajax({
      url: my_url,
      type: 'POST',
      dataType: 'json',
      data: formData,
      success: function(data)
      {

          var db_select="";
        var name = "Không có dữ liệu";
         var tongtien_theohoadon=0;
         var tongtien_trahang=0;
           for (var key in data.theohoadon) {
                if (data.theohoadon.hasOwnProperty(key)) {
                  //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                       
                        db_select+= '<tr>';                        
                        db_select+= "<td>" + data.theohoadon[key]['KH_TEN'] + "</td>";
                        db_select+= '<td>' + data.theohoadon[key]['TD_TEN'] + "</td>";
                        db_select+= '<td>' + data.theohoadon[key]['name']  + "</td>";                                                
                        db_select+= '<td class="tiente text-right">' + data.theohoadon[key]['THANHTIEN'] 
                                  + '</td>';
                        db_select+= '</tr>';
                        tongtien_theohoadon+=parseInt(data.theohoadon[key]['THANHTIEN']);
                    }

                  }//endfor 
                $('#datatable_theohoadon tbody').html(db_select); 
                $('#tongtien_theohoadon').html('<span  class="tiente">'+tongtien_theohoadon+'</span>'); 
                
                db_select="";
              for (var key in data.trahang) {
                if (data.trahang.hasOwnProperty(key)) {
                  //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                       
                        db_select+= '<tr>';                        
                        db_select+= "<td>" + data.trahang[key]['KH_TEN'] + "</td>";
                        db_select+= '<td>' + data.trahang[key]['TD_TEN'] + "</td>";
                        db_select+= '<td>' + data.trahang[key]['name']  + "</td>";                                                
                        db_select+= '<td class="tiente text-right">' + data.trahang[key]['TIENTRA'] 
                                  + '</td>';
                        db_select+= '</tr>';
                        tongtien_trahang+=parseInt(data.trahang[key]['TIENTRA']);
                    }

                  }//endfor     
                  $('#datatable_trahang tbody').html(db_select);  
                  $('#tongtien_trahang').html('<span   class="tiente">'+tongtien_trahang+'</span>');

                  
                  //tinh doanh thu thuc te 
                  var dttt = tongtien_theohoadon-tongtien_trahang;
                  $('#doanhthuthucte').html('<span  class="tiente blue">'+dttt+'</span>');
                  setautoNumeric('.tiente','tiente');
      },
      error:function(data)
      {

      }


    });
  });
  /* ******* */

  /*  click #btn-donhang xem chi tiet don hang theo khach hang */
  $(document).on('click','.btn-donhang',function(){
        var   id_DH = $(this).val();
        var   my_url= url+'/load/donhang/'+id_DH;        
        $('#donhang_id').val(id_DH);
        $(modal).one('shown.bs.modal',function(){

        $.get(my_url,function(data){
          //console.log(data);
          var db_select="";
          var name = "Không có dữ liệu";
          status_kho=true;
           for (var key in data) 
           {
                if (data.hasOwnProperty(key)) {
                  //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                        var red ='';
                        var tru ='red';
                        var status ="Đã trừ kho";
                        if(data[key]['SOLUONG']>data[key]['SLKHO'])
                        {
                           status_kho = false;
                           red='red';
                        }
                        if(data[key]['CTDH_STATUS']==0)
                        { 
                          tru = ' ';
                          status ="Chưa Trừ Kho";
                        }
                        db_select+= '<tr>';
                        db_select+= '<td>' + (Number(key)+1) + '</td>';
                        db_select+= "<td>" + data[key]['HH_TEN']  + "-" + data[key]['QC_TEN'] + "</td>";
                        db_select+= '<td class="'+tru+'">' + data[key]['SOLUONG'] + "</td>";
                        db_select+= '<td class="'+red+'">' + data[key]['SLKHO'] +  "</td>";
                        db_select+= '<td class="tiente">' + data[key]['DONGIA'] +  "</td>";                        
                        db_select+= '<td class="tiente">' + Number(data[key]['SOLUONG'])*Number(data[key]['DONGIA']) + "</td>";
                        db_select+= '<td>' +  '<button type="button" class="btn btn-primary btn-xs btn-edit" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-edit"></i></button>' 
                                           +  '<button type="button" class="btn btn-danger  btn-xs btn-xoa" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-trash"></i></button> <i>'+ status +'</i>' + "</td>";
                        db_select+= '</tr>';
                    }

            }//endfor            
           $('#datatable_chitiet tbody').html(db_select);
           setautoNumeric('.tiente','tiente');
        }); //end get value        

      });// end shoqn.bs.modal
      $(modal).modal();
  });

  /* ******* */
  /* click vao nut edit trong dialog  danh sach  bang hang cua khach hang */
  $(document).on('click','.btn-edit',function(){
      $('#frmchitiet').trigger('reset');       
      var  id_CTDH = $(this).val();
      $(modal_edit).one('shown.bs.modal',function(){
            
          var  my_url = url + '/load/chitietdonhang/'+ id_CTDH;
          $.get(my_url,function(data){
              //console.log(data[0].HH_TEN);
              $('#txtTen').val(data[0].HH_TEN + '-' + data[0].QC_TEN );
              $('#txtSoluong').val(data[0].SOLUONG);
              $('#txtDongia').val(data[0].DONGIA);
              $('#chitietdonhang_id').val(data[0].CTDH_ID);
          });
          setautoNumeric('.tiente','tiente');
      });
      $(modal_edit).modal();
  });
  /* ******* */
  /* click vao nut  luu trong dialog edit chitietdonhang */
  $(document).on('click','#btn-save-chitiet',function(){

      var id_CTDH = $('#chitietdonhang_id').val();
      var  my_url = url + /chitietdonhang/ + id_CTDH;

      formData = {
        'SOLUONG' : $('#txtSoluong').val(),
        'DONGIA'  : $('#txtDongia').autoNumeric('get'),
      };

      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        }); 
        $.ajax({
            type: 'PUT',
            url: my_url,            
            data: formData,           
            dataType: 'json',            
            success: function (data) {
              //console.log(data);
              var id_DH = $('#donhang_id').val();
              var   my_url= url+'/load/donhang/'+id_DH;
              $.get(my_url,function(data){
            
                  var db_select="";
                  var name = "Không có dữ liệu";
                  status_kho=true;
                   for (var key in data) 
                   {
                        if (data.hasOwnProperty(key)) {
                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                var red ='';
                                var tru ='red';
                                var status ="Đã trừ kho";
                                if(data[key]['SOLUONG']>data[key]['SLKHO'])
                                {
                                   status_kho = false;
                                   red='red';
                                }
                                if(data[key]['CTDH_STATUS']==0)
                                { 
                                  tru = ' ';
                                  status ="Chưa Trừ Kho";
                                }
                                db_select+= '<tr>';
                                db_select+= '<td>' + (Number(key)+1) + '</td>';
                                db_select+= "<td>" + data[key]['HH_TEN']  + "-" + data[key]['QC_TEN'] + "</td>";
                                db_select+= '<td class="'+tru+'">' + data[key]['SOLUONG'] + "</td>";
                                db_select+= '<td class="'+red+'">' + data[key]['SLKHO'] +  "</td>";
                                db_select+= '<td class="tiente">' + data[key]['DONGIA'] +  "</td>";                        
                                db_select+= '<td class="tiente">' + Number(data[key]['SOLUONG'])*Number(data[key]['DONGIA']) + "</td>";
                                db_select+= '<td>' +  '<button type="button" class="btn btn-primary btn-xs btn-edit" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-edit"></i></button>' 
                                                   +  '<button type="button" class="btn btn-danger  btn-xs btn-xoa" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-trash"></i></button> <i>'+ status +'</i>' + "</td>";
                                db_select+= '</tr>';
                            }

                    }//endfor                   
                 $('#datatable_chitiet tbody').html(db_select);
                 setautoNumeric('.tiente','tiente');
                 $(modal_edit).modal('hide');
              });

            }, // end   ajax  post xử lý dữ liệu xong
            error: function (data) {
                //console.log('Error:', data); // thông báo lỗi qua console 
            }
        });
  });

  /* ******* */
  /* click vao nut duyet trong dialog danh sach hang ban */
  $(document).on('click','#btn-duyet',function(){

       var id_DH = $('#donhang_id').val();
       var my_url = url + '/duyet/'+id_DH;
       var formData = {

          DH_DUYET : 1 

       };
        if(status_kho)
        {
          $.ajaxSetup({
           headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        }); 
        $.ajax({
            type: 'PUT',
            url: my_url,            
            data: formData,           
            dataType: 'json',            
            success: function (data) {
                //console.log(data);
                id_NV = $('#cmbNV').val();
                if(id_NV=='') {id_NV='all'}
                 day =  $('#single_cal2').val();
                var my_url = url + '/user/'+id_NV+'/"'+ day + '"';
                $.get(my_url,function(data){
                     //console.log(data);
                    var db_select="";
                    var name = "Không có dữ liệu";
                       for (var key in data) {
                            if (data.hasOwnProperty(key)) {
                              //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                    var duyet = ' ';
                                    if(data[key]['DH_DUYET']==1) {duyet='green';}
                                    db_select+= '<tr>';                        
                                    db_select+= "<td>" + data[key]['KH_TEN'] + "</td>";
                                    db_select+= '<td>' + data[key]['TD_TEN'] + "</td>";
                                    db_select+= '<td>' + data[key]['name']  + "</td>";                                                
                                    db_select+= '<td>' +  '<button type="button" class="btn btn-info btn-xs btn-donhang" value="'+data[key]['DH_ID']+'" ><i class="fa fa-eye"></i></button>' 
                                                       +  '<button type="button" class="btn btn-default btn-xs '+ duyet +' "> <i class="fa fa-check"></i></button>'
                                                       +  '<button type="button"  class="btn btn-xs btn-danger" id="btn-xoa-donhang" value="'+data[key]['DH_ID']+'" data-toggle="tooltip" title="Xóa đơn hàng"> <i class="fa fa-close"></i></button>'
                                              + '</td>';
                                    db_select+= '</tr>';
                                }
                              }//endfor 
                  $('#datatable tbody').html(db_select);

                              
               });
              $(modal).modal('hide');
              }, // end   ajax  post xử lý dữ liệu xong
            error: function (data) {
                //console.log('Error:', data); // thông báo lỗi qua console 
            }
        }); //end ajax
            
        }
        else
        {
           alert('!- Một hoặc nhiều hàng không đủ  số Lượng  trong Kho | Vui lòng kiểm tra lại') ;
        }
        
  });

  /* ******* */ 
  /* click btn xoa trong dialog chitietdonhang */
  $(document).on('click','.btn-xoa',function(){
      var id_CTDH = $(this).val();

       var my_url = url+ '/chitietdonhang/'+id_CTDH;
        if (confirm("Click Ok Nếu bạn chấp nhận xóa!") == true) {
            
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        });
       $.ajax({

          type: 'DELETE',
            url: my_url,            
            dataType: 'json',            
            success: function (data) {
                //console.log(data);
                var id_DH = $('#donhang_id').val();
                var   my_url= url+'/load/donhang/'+id_DH;
                $.get(my_url,function(data){
            
                  var db_select="";
                  var name = "Không có dữ liệu";
                  status_kho=true;
                   for (var key in data) 
                   {
                        if (data.hasOwnProperty(key)) {
                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                var red ='';
                                var tru ='red';
                                var status ="Đã trừ kho";
                                if(data[key]['SOLUONG']>data[key]['SLKHO'])
                                {
                                   status_kho = false;
                                   red='red';
                                }
                                if(data[key]['CTDH_STATUS']==0)
                                { 
                                  tru = ' ';
                                  status ="Chưa Trừ Kho";
                                }
                                db_select+= '<tr>';
                                db_select+= '<td>' + (Number(key)+1) + '</td>';
                                db_select+= "<td>" + data[key]['HH_TEN']  + "-" + data[key]['QC_TEN'] + "</td>";
                                db_select+= '<td class="'+tru+'">' + data[key]['SOLUONG'] + "</td>";
                                db_select+= '<td class="'+red+'">' + data[key]['SLKHO'] +  "</td>";
                                db_select+= '<td class="tiente">' + data[key]['DONGIA'] +  "</td>";                        
                                db_select+= '<td class="tiente">' + Number(data[key]['SOLUONG'])*Number(data[key]['DONGIA']) + "</td>";
                                db_select+= '<td>' +  '<button type="button" class="btn btn-primary btn-xs btn-edit" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-edit"></i></button>' 
                                                   +  '<button type="button" class="btn btn-danger  btn-xs btn-xoa" value="'+data[key]['CTDH_ID']+'"> <i class="fa fa-trash"></i></button> <i>'+ status +'</i>' + "</td>";
                                db_select+= '</tr>';
                            }

                    }//endfor              
                 $('#datatable_chitiet tbody').html(db_select);
                 setautoNumeric('.tiente','tiente');           
              });

            }, // end   ajax  post xử lý dữ liệu xong
            error: function (data) {
                //console.log('Error:', data); // thông báo lỗi qua console 
            }
       });


        } else {
            
            return false;
        }

  });

  /* ******* */
  /* btn in */
  $(document).on('click','#btn-in',function(){
     var id_NV = $('#cmbNV').val();
     var id_DH = $('')
     var ten_NV = $('#cmbNV option:selected').html()
     var day =  $('#single_cal2').val();
    if(id_NV!="")
    {
      if(confirm('Bạn muốn In ngày '+day+ ' của nhân viên '+ten_NV)==true)
      {    var my_url = '/public/in/donhanga5/'+day+'/'+id_NV
          window.open(my_url,'_blank');
      }   

    }
    else
    {
       show_stack_bottomright('error');
    }
    
  });
  /* ******* */
  /* btn xoa 1 don hang */
  $(document).on('click','#btn-xoa-donhang',function(){
      var id_DH = $(this).val();
      var my_url = url+"/xoadonhang/"+id_DH;
      if(confirm('Bạn muốn xóa đơn hàng này')==true)
      {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
              }
          });
        $.ajax({
           url: my_url,
           type: 'DELETE',
           success: function(data)
          {
            id_NV = $('#cmbNV').val();
            if(id_NV=='') {id_NV='all'}
             day =  $('#single_cal2').val();
            var my_url = url + '/user/'+id_NV+'/'+ day ;
            $.get(my_url,function(data){
                 //console.log(data);
                var db_select="";
                var name = "Không có dữ liệu";
                   for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                var duyet = ' ';
                                if(data[key]['DH_DUYET']==1) {duyet='green';}
                                db_select+= '<tr>';                        
                                db_select+= "<td>" + data[key]['KH_TEN'] + "</td>";
                                db_select+= '<td>' + data[key]['TD_TEN'] + "</td>";
                                db_select+= '<td>' + data[key]['name']  + "</td>";                                                
                                db_select+= '<td>' +  '<button type="button" class="btn btn-info btn-xs btn-donhang" value="'+data[key]['DH_ID']+'" ><i class="fa fa-eye"></i></button>' 
                                                   +  '<button type="button" class="btn btn-default btn-xs '+ duyet +' "> <i class="fa fa-check"></i></button>'
                                                   +  '<button type="button"  class="btn btn-xs btn-danger" id="btn-xoa-donhang" value="'+data[key]['DH_ID']+'" data-toggle="tooltip" title="Xóa đơn hàng"> <i class="fa fa-close"></i></button>'
                                          + '</td>';
                                db_select+= '</tr>';
                            }
                          }//endfor 
                          $('#datatable tbody').html(db_select);                  
                          show_stack_bottomright('success');
             });
          },
           error:function(data)
           {

           }        

        });


      }
 

  });

});

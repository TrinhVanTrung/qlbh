$(document).ready(function(){

    var url = "/public/admin/hanghoa"; // dinh huong url  cho ajax 
    var name ='HÀNG HOÁ';
    var  idmodal = '#hanghoaModal';
    var  idfrm = '#frmHangHoa';
    var  id_ma = '#hanghoa_id';
    
    setDataTable('#datatable');
    //display modal form for task editing,  mo  modal chua' form them
    //  load dữ liệu vào modal form này.
    $(document).on('click','.open-modal',function(e){
       e.preventDefault();
        var id = $(this).val(); // id nằm trong btn có class .open-mal
        $.get(url + '/' + id, function (data) {
            //success data
            console.log(data);
            $(id_ma).val(data.HH_ID);
            $('#txtTen').val(data.HH_TEN);
            
            $('#btn-save').val("update"); // chuyển value về trạng thái  'update'
            
            addDataForSelect('.select_quycach','/public/admin/quycach/allresponse','QC_ID','QC_TEN',data.ID_QC,'');
            addDataForSelect('.select_nhomhang','/public/admin/nhomhang/allresponse','NH_ID','NH_TEN',data.ID_NH,'');
            addDataForSelect('.select_dvt','/public/admin/donvitinh/allresponse','DVT_ID','DVT_TEN',data.ID_DVT,'');
            $(idmodal).modal();
        }) 
    });

    //display modal form for creating new task
     $(document).on('click','#btn-add',function(){
        $('#btn-save').val("add"); // chuyển value về trạng thái  add
        $(idfrm).trigger("reset"); // reset form
        $(idmodal+' .modal-footer div.alert').addClass('hidden');  // 
        //add data cho select quy cach
        addDataForSelect('.select_quycach','/public/admin/quycach/allresponse','QC_ID','QC_TEN',"",'');
        addDataForSelect('.select_nhomhang','/public/admin/nhomhang/allresponse','NH_ID','NH_TEN',"",'');
        addDataForSelect('.select_dvt','/public/admin/donvitinh/allresponse','DVT_ID','DVT_TEN',"",'');
        $(idmodal).modal();
    });

    //delete task and remove it from list
    //****************** XOA RECORD *************************
    
    $(document).on('click','.delete',function(e){
        var id = $(this).val();
        
           $('#delModal').on('show.bs.modal', function(){
             $('#delModal').find('button.btnXoa').attr('id','btnxoa'+id);
             $('#delModal').find('.modal-title').text('THÔNG BÁO');
             $('#delModal').find('.modal-body').text('Bạn có thật sự muốn xoá ');
           });
            $('#delModal').modal('show');
            $(document).on('click','#btnxoa'+id,function(e) {
                e.preventDefault();
                 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                });
                $.ajax({
                    type: "DELETE",
                    url: url + '/' +id,
                    success: function (data) {
                    
                    //    console.log(data);
                       $('#delModal').on('hidden.bs.modal',function(){
                            
                            $('.right_col').load(url+'/all',function(){
                             setDataTable('#datatable');
                             show_stack_bottomright('success');
                                });
                            
                       });
                    
                     $("#delModal").modal("hide");
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
             });
            
    });
        
    //******************END  XOA RECORD *********************
    
    
    
    //create new task / update existing task
    //****************** THÊM VÀ CẬP NHẬT RECORD *************************
    $(document).on('click','#btn-save',function(e) {
        e.preventDefault();
        // bay loi nhap vao 
        if($('#txtTen').val()=="" || $('#cmbQuycach').val()=="" || $('#cmbNhomhang').val()=="" || $('#cmbDonvitinh').val()=="")
        {
            var str_alert = "<strong>Không bỏ trống !</strong> Không được bỏ trống, mọi thứ phải được điền hoặc chọn.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
                    
            return false;
        }
        
        // lay du lieu tu formvao , luu Y: Truong nhan du lieu vao la truong giong voi field trong table
        // trim(): xoa khoan trang du thua.
        var formData = {
            HH_TEN: $('#txtTen').val().trim(),
            ID_DVT: $('#cmbDonvitinh').val(),
            ID_NH : $('#cmbNhomhang').val(),
            ID_QC : $('#cmbQuycach').val(),
           }
        
        //used to determine the http verb to use [add=POST], [update=PUT]
        // kiểm tra thêm mới hay cập nhật
        var state = $('#btn-save').val(); // trong nút có attribute có value

        var type = 'POST'; //for creating new resource
        var id = $(id_ma).val(); // trong nút có id là id của record,
        var my_url = url;

        if (state == 'update'){
            type = 'PUT'; //for updating existing resource
            my_url += '/' + id;
        }
        console.log(formData);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        }); 
        $.ajax({
            type: type,
            url: my_url,            
            data: formData,           
            dataType: 'json',            
            success: function (data) {
                console.log(data);
                if(data.error=='idisset') // trường hợp trùng id
                {   
                    var str_alert = "<strong>Có rồi!</strong> Mã vừa nhập đã có  rồi hãy nhập mà mới.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
                    
                }
                else
                {
                    $(idfrm).trigger("reset");
                    $(idmodal).on('hidden.bs.modal',function(){
                            $('.right_col').load(url+'/all',function(){
                              setDataTable('#datatable');
                             show_stack_bottomright('success');
                                });
                    });
                    
            $(idmodal).modal('hide');
                }// end kiểm tra tồn tại id  và thêm mới 
            }, // end   ajax  post xử lý dữ liệu xong
            error: function (data) {
                console.log('Error:', data); // thông báo lỗi qua console 
            }
        });
    });
    //******************END THÊM VÀ CẬP NHẬT RECORD *************************
});

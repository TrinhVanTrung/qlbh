$(document).ready(function(){

	var url = '/public/admin/trahang';
  var modal = '#trahangModal';
  var  modal_edit = '#chitietdonhangModal';
  var  day = "";
  var  id_NV ="";
  var  status_mahang = false;
  
	addDataForSelect('#cmbHanghoa','/public/admin/hanghoa/allresponse','HH_ID','HH_TEN','','QC_TEN');
  setautoNumeric('#txtDongia','tiente');
	
	$(document).on('click','#btn-search',function(){
      var idDH = $('#txtMadonhang') .val();
      if(idDH=='')
      {
         show_stack_bottomright('error');
         $('#txtMadonhang').focus();
         return false;
      }
     var my_url = url+ '/load/donhang/'+idDH;
     $.get(my_url,function(data){
         if(data[0])
         {
          $('#id_DH').html(data[0].DH_ID + ' tồn tại'); 
          status_mahang=true;
          var   my_url= url+'/'+data[0].DH_ID;
              $.get(my_url,function(data){
                console.log(data);
                  var db_select="";
                  var name = "Không có dữ liệu";
                  
                   for (var key in data) 
                   {
                        if (data.hasOwnProperty(key)) {
                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                                               
                                db_select+= '<tr>';
                                db_select+= "<td>" + data[key]['HH_TEN']  + "-" + data[key]['QC_TEN'] + "</td>";
                                db_select+= '<td class="">' + data[key]['SOLUONG'] + "</td>";
                                db_select+= '<td class="tiente">' + data[key]['DONGIA'] +  "</td>";                        
                                db_select+= '<td class="tiente">' + Number(data[key]['SOLUONG'])*Number(data[key]['DONGIA']) + "</td>";
                                db_select+= '<td>'  +  '<button type="button" class="btn btn-danger  btn-xs btn-xoa" value="'+data[key]['TH_ID']+'"> <i class="fa fa-trash"></i></button> <i>'+ status +'</i>' + "</td>";
                                db_select+= '</tr>';
                            }

                    }//endfor                   
                 $('#datatable tbody').html(db_select);
                 setautoNumeric('.tiente','tiente');
              });
         }
         else
         {
          alert('Không tìm thấy mã phù hợp - Vui lòng kiểm tra lại');
          $('#id_DH').html('');
          $('#txtMadonhang').val('');
         }         
     });
		
  });
  /* ******* */

  
  $(document).on('click','#btn-trahang',function(){
        var   id_DH = $('#txtMadonhang').val();
        var   id_HH = $('#cmbHanghoa').val();
        var   soluong = $('#txtSoluong').val();
        var   dongia = $('#txtDongia').autoNumeric('get');
        var   ghichu = $('#txtGhichu').val();
        var my_url  = url;
        if(id_DH=="" || id_HH=="" || soluong=="" || dongia =="" )
        {
           show_stack_bottomright('error');
           return false;
        }
        var formData = {
          'ID_DH'   : id_DH,
          'ID_HH'   : id_HH,
          'SOLUONG' : soluong,
          'DONGIA'  : dongia,
          'GHICHU'  : ghichu
        };

         $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
          });        
        
         $.ajax({
          type: 'POST',
           url: my_url,           
           dataType: 'json',
           data: formData,
           success: function(data){
             //console.log(data);
              var   my_url= url+'/'+id_DH;
              $.get(my_url,function(data){
                console.log(data);
                  var db_select="";
                  var name = "Không có dữ liệu";
                  
                   for (var key in data) 
                   {
                        if (data.hasOwnProperty(key)) {
                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                                               
                                db_select+= '<tr>';
                                db_select+= "<td>" + data[key]['HH_TEN']  + "-" + data[key]['QC_TEN'] + "</td>";
                                db_select+= '<td class="">' + data[key]['SOLUONG'] + "</td>";
                                db_select+= '<td class="tiente">' + data[key]['DONGIA'] +  "</td>";                        
                                db_select+= '<td class="tiente">' + Number(data[key]['SOLUONG'])*Number(data[key]['DONGIA']) + "</td>";
                                db_select+= '<td>'  +  '<button type="button" class="btn btn-danger  btn-xs btn-xoa" value="'+data[key]['TH_ID']+'"> <i class="fa fa-trash"></i></button> <i>'+ status +'</i>' + "</td>";
                                db_select+= '</tr>';
                            }

                    }//endfor                   
                 $('#datatable tbody').html(db_select);
                 setautoNumeric('.tiente','tiente');
              });
           },
           error:function(data){
            console.log(data);
           },
         });
  
  });


  /* ******* */ 
  /* click btn xoa trong dialog chitietdonhang */
  $(document).on('click','.btn-xoa',function(){
      var id_TH = $(this).val(); //id tra hang

       var my_url = url+ '/'+id_TH;
        if (confirm("Click Ok Nếu bạn chấp nhận xóa!") == true) {
            
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        });
       $.ajax({

          type: 'DELETE',
            url: my_url,            
            dataType: 'json',            
            success: function (data) {
                //console.log(data);
              id_DH = $('#txtMadonhang').val();
              var   my_url= url+'/'+id_DH;
              $.get(my_url,function(data){
                console.log(data);
                  var db_select="";
                  var name = "Không có dữ liệu";
                  
                   for (var key in data) 
                   {
                        if (data.hasOwnProperty(key)) {
                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                                               
                                db_select+= '<tr>';
                                db_select+= "<td>" + data[key]['HH_TEN']  + "-" + data[key]['QC_TEN'] + "</td>";
                                db_select+= '<td class="">' + data[key]['SOLUONG'] + "</td>";
                                db_select+= '<td class="tiente">' + data[key]['DONGIA'] +  "</td>";                        
                                db_select+= '<td class="tiente">' + Number(data[key]['SOLUONG'])*Number(data[key]['DONGIA']) + "</td>";
                                db_select+= '<td>'  +  '<button type="button" class="btn btn-danger  btn-xs btn-xoa" value="'+data[key]['TH_ID']+'"> <i class="fa fa-trash"></i></button> <i>'+ status +'</i>' + "</td>";
                                db_select+= '</tr>';
                            }

                    }//endfor                   
                 $('#datatable tbody').html(db_select);
                 setautoNumeric('.tiente','tiente');
              });

            }, // end   ajax  post xử lý dữ liệu xong
            error: function (data) {
                //console.log('Error:', data); // thông báo lỗi qua console 
            }
       });


        } else {
            
            return false;
        }

  });

  /* ******* */
  /* btn in */
  $(document).on('click','#btn-in',function(){
     var id_NV = $('#cmbNV').val();
     var id_DH = $('')
     var ten_NV = $('#cmbNV option:selected').html()
     var day =  $('#single_cal2').val();
    if(id_NV!="")
    {
      if(confirm('Bạn muốn In ngày '+day+ ' của nhân viên '+ten_NV)==true)
      {    var my_url = '/public/in/donhanga5/'+day+'/'+id_NV
          window.open(my_url,'_blank');
      }   

    }
    else
    {
       show_stack_bottomright('error');
    }
    
  });
  /* ******* */
  /* btn xoa 1 don hang */
  $(document).on('click','#btn-xoa-donhang',function(){
      var id_DH = $(this).val();
      var my_url = url+"/xoadonhang/"+id_DH;
      if(confirm('Bạn muốn xóa đơn hàng này')==true)
      {
          $.ajaxSetup({
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
              }
          });
        $.ajax({
           url: my_url,
           type: 'DELETE',
           success: function(data)
          {
            id_NV = $('#cmbNV').val();
            if(id_NV=='') {id_NV='all'}
             day =  $('#single_cal2').val();
            var my_url = url + '/user/'+id_NV+'/'+ day ;
            $.get(my_url,function(data){
                 //console.log(data);
                var db_select="";
                var name = "Không có dữ liệu";
                   for (var key in data) {
                        if (data.hasOwnProperty(key)) {
                          //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                                var duyet = ' ';
                                if(data[key]['DH_DUYET']==1) {duyet='green';}
                                db_select+= '<tr>';                        
                                db_select+= "<td>" + data[key]['KH_TEN'] + "</td>";
                                db_select+= '<td>' + data[key]['TD_TEN'] + "</td>";
                                db_select+= '<td>' + data[key]['name']  + "</td>";                                                
                                db_select+= '<td>' +  '<button type="button" class="btn btn-info btn-xs btn-donhang" value="'+data[key]['DH_ID']+'" ><i class="fa fa-eye"></i></button>' 
                                                   +  '<button type="button" class="btn btn-default btn-xs '+ duyet +' "> <i class="fa fa-check"></i></button>'
                                                   +  '<button type="button"  class="btn btn-xs btn-danger" id="btn-xoa-donhang" value="'+data[key]['DH_ID']+'" data-toggle="tooltip" title="Xóa đơn hàng"> <i class="fa fa-close"></i></button>'
                                          + '</td>';
                                db_select+= '</tr>';
                            }
                          }//endfor 
                          $('#datatable tbody').html(db_select);                  
                          show_stack_bottomright('success');
             });
          },
           error:function(data)
           {

           }        

        });


      }
 

  });

});

$(document).ready(function(){
    
    var url = "/public/admin/kho/nhapkho"; // dinh huong url  cho ajax 
    var name ='THÊM HÀNG';
    var  idmodal = '#themhangModal';
    var  idfrm = '#frmThemHang';
    var  id_ma = '#hanghoa_id';
    var id_PN= $('#id_PN').val();
    showDay('#ngaythang');
    
    if(id_PN==0) // tuc la moi khoi tao
    {   // tao moi id
         id_PN = taoIdPhieuNhapXuat('PN');
         $('#id_PN').html(id_PN); 
         $('#id_PN').val(id_PN);
    }

    setDataTable('#datatable');
    $(document).on('click','.open-modal',function(e) {
        $('#btn-save').val('update');
        var id_HH = $(this).val();
        var sl = $('#sl'+id_HH).html();
        $('#txtSoluong').html(sl);
        $(id_ma).val(id_HH);
        $(idmodal).one('show.bs.modal',function(){
            addDataForSelect('.select_hanghoa','/public/admin/hanghoa/allresponse','HH_ID','HH_TEN',id_HH,'QC_TEN');
        });
         
         $(".select_hanghoa").prop("disabled", true); // disable select hang hoa
         
         
         $(idmodal).modal();
        
    });
   
    $(document).on('click','#btn-add',function(e) {
        $('#btn-save').val("add"); // chuyển value về trạng thái  add
        $(".select_hanghoa").prop("disabled", false);
        $(idfrm).trigger("reset"); // reset form
        $(idmodal+' .modal-footer div.alert').addClass('hidden');  // 
        $(idmodal).one('show.bs.modal',function(){
            addDataForSelect('.select_hanghoa','/public/admin/hanghoa/allresponse','HH_ID','HH_TEN','','QC_TEN');    
        });
        $(idmodal).modal();
    });
    
    $(document).on('click','#btn-save',function(e) {
        e.preventDefault();
        // bay loi nhap vao 
        if($('#txtSoluong').val()=="" || $('#cmbHanghoa').val()=="")
        {
             var str_alert = "<strong>Không bỏ trống !</strong> Không được bỏ trống, mọi thứ phải được điền hoặc chọn.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
            return false;
        }
        
        // lay du lieu tu formvao , luu Y: Truong nhan du lieu vao la truong giong voi field trong table
        
        var formData = {
            SOLUONG: $('#txtSoluong').val(),
            HH_ID: $('#cmbHanghoa').val(),
            PN_ID : id_PN,
           }
        
        //used to determine the http verb to use [add=POST], [update=PUT]
        // kiểm tra thêm mới hay cập nhật
        var state = $('#btn-save').val(); // trong nút có attribute có value

        var type = 'POST'; //for creating new resource
        var id = $(id_ma).val(); // trong nút có id là id của record,
        var my_url = url;

        if (state == 'update'){
            type = 'PUT'; //for updating existing resource
            my_url += '/' + id;
        }
        //console.log(formData);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        }); 
        $.ajax({
            type: type,
            url: my_url,            
            data: formData,           
            dataType: 'json',            
            success: function (data) {
                console.log(data);
                if(data.error=='idisset') // trường hợp trùng id
                {   
                    var str_alert = "<strong>Có rồi!</strong> Mã vừa nhập đã có  rồi hãy nhập mà mới.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
                    
                }
                else
                {
                    $(idfrm).trigger("reset");
                    $(idmodal).on('hidden.bs.modal',function(){
                            $('.right_col').load(url+'/'+id_PN,function(){
                              setDataTable('#datatable');
                              showDay('#ngaythang');
                             show_stack_bottomright('success');
                                });
                    });
                    
            $(idmodal).modal('hide');
                }// end kiểm tra tồn tại id  và thêm mới 
            }, // end   ajax  post xử lý dữ liệu xong
            error: function (data) {
                console.log('Error:', data); // thông báo lỗi qua console 
            }
        });
    });
    
    $(document).on('click','#btn-save-on',function(){
        
        //1. set gia tri cho SAVE_OFF =1
        $.ajax({
            type:'get',
            url: url+"/setsaveoff/"+id_PN,
            dataType: 'json',
            success:function(data){
                console.log(data);
                 $(window).off("beforeunload");
                show_stack_bottomright('success');
                window.location.replace("/public/admin/kho");
               
            },
            error:function(data){
                console.log('Error:', data);
            }
            
            
        });
       
       
                
        
    });
    
    //code js load du lieu lich su nhap kho cho tung san pham
    $(document).on('click','.open-modal',function(e){
        e.preventDefault();
         var id = $(this).val();
        
        $('#lichsunhapModal').one('show.bs.modal',function(){
        //get du lieu 
        $.get(url+'/'+id,function(data){
           //console.log(data);
           var db_select="";
            var name = "Không có dữ liệu";
           for (var key in data) {
                if (data.hasOwnProperty(key)) {
                  //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                        db_select+= '<tr>';
                        db_select+= "<td> " + data[key]['NGAYNHAP'] + "</td>";
                        db_select+= '<td>' + data[key]['SOLUONG'] + "</td>";
                        db_select+= '</tr>';
                    }
            }
            
           $('#list').html(db_select);
                });
            
        });
        $('#lichsunhapModal').modal();
        
        
    });
    $(document).on('click','.delete',function(e){
                var id = $(this).val();
               
                         $('#delModal').on('show.bs.modal', function(){
             $('#delModal').find('button.btnXoa').attr('id','btnxoa'+id);
             $('#delModal').find('.modal-title').text('THÔNG BÁO');
             $('#delModal').find('.modal-body').text('Bạn có thật sự muốn xoá !!');
          });
            $('#delModal').modal();
            $(document).on('click','#btnxoa'+id,function(e) {
                e.preventDefault();
                 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                });
                $.ajax({
                    type: "DELETE",
                    url: url + '/' +id+ '/'+ id_PN,
                    success: function (data) {
                        console.log(data);
                      $('#delModal').on('hidden.bs.modal',function(){
                            $('.right_col').load(url+'/'+id_PN,function(){
                              setDataTable('#datatable');
                              showDay('#ngaythang');
                              show_stack_bottomright('success');
                                });
                            });
                    
                     $("#delModal").modal("hide");
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
             });
            
    });
});


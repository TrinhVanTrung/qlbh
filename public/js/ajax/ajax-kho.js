$(document).ready(function(){

    var url = "/public/admin/kho/lichsunhap"; // dinh huong url  cho ajax 
    var name ='ĐƠN VỊ TÍNH';
    var  idmodal = '#donvitinhModal';
    var  idfrm = '#frmDonvitinh';
    var  id_ma = '#donvitinh_id';
    
    setDataTable('#datatable');
    
    
    //code js load du lieu lich su nhap kho cho tung san pham
    $(document).on('click','.open-modal',function(e){
        e.preventDefault();
         var id = $(this).val();
        
        $('#lichsunhapModal').one('show.bs.modal',function(){
        //get du lieu 
        $.get(url+'/'+id,function(data){
           //console.log(data);
           var db_select="";
            var name = "Không có dữ liệu";
           for (var key in data) {
                if (data.hasOwnProperty(key)) {
                  //console.log(data[key]["QC_ID"] + ", " + data[key]["QC_TEN"]);
                        db_select+= '<tr>';
                        db_select+= "<td> " + data[key]['NGAYNHAP'] + "</td>";
                        db_select+= '<td>' + data[key]['SOLUONG'] + "</td>";
                        db_select+= '</tr>';
                    }
            }
            
           $('#list').html(db_select);
                });
            
        });
        $('#lichsunhapModal').modal();
        
        
    });
    $(document).on('click','#btnXoa',function(){
        
        $('#lichsunhapModal').one('hidden.bs.modal',function(){
        
            $('#list').html("");
        });
    $('#lichsunhapModal').modal('hide');
        
    });
});



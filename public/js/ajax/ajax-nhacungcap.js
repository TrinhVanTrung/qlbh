$(document).ready(function(){

    setDataTable('#datatable');
    var url = "/public/admin/nhacungcap"; // dinh huong url  cho ajax
    var name ='NHÀ CUNG CẤP';
    var  idmodal = '#nhacungcapModal';
    var  idfrm = '#frmNhaCungCap';
    var  id_ma = '#nhacungcap_id';
    

    //display modal form for task editing,  mo  modal chua' form them
    //  load dữ liệu vào modal form này.
    $(document).on('click','.open-modal',function(e){
       e.preventDefault();
        var id = $(this).val(); // id nằm trong btn có class .open-mal
        $.get(url + '/' + id, function (data) {
            //success data
            console.log(data[0]);
            $(id_ma).val(data[0].NCC_ID);
            $('#txtTen').val(data[0].NCC_TEN);
            $('#txtSDT').val(data[0].NCC_SDT);
            $('#txtDiachi').val(data[0].NCC_DIACHI);
            $('#btn-save').val("update"); // chuyển value về trạng thái  'update'
            $(idmodal).modal();
        }) 
    });

    //display modal form for creating new task
     $(document).on('click','#btn-add',function(){
        $('#btn-save').val("add"); // chuyển value về trạng thái  add
        $(idfrm).trigger("reset"); // reset form
        $(idmodal+' .modal-footer div.alert').addClass('hidden');  // 
        $(idmodal).modal();
    });

    //delete task and remove it from list
    //****************** XOA RECORD *************************
    
    $(document).on('click','.delete',function(e){
        var id = $(this).val();
        
            $('#delModal').on('show.bs.modal', function(){
             $('#delModal').find('button.btnXoa').attr('id','btnxoa'+id);
             $('#delModal').find('.modal-title').text('THÔNG BÁO');
             $('#delModal').find('.modal-body').text('Bạn có thật sự muốn xóa');
           });
            $('#delModal').modal('show');
            $(document).on('click','#btnxoa'+id,function(e) {
                e.preventDefault();
                 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                });
                $.ajax({
                    type: "DELETE",
                    url: url + '/' +id,
                    success: function (data) {
                        console.log(data);
                       $('#delModal').on('hidden.bs.modal',function(){
                            $('.right_col').load(url+'/all',function(){
                             setDataTable('#datatable');
                             show_stack_bottomright('success');
                                });
                            });
                    
                     $("#delModal").modal("hide");
                    },
                    error: function (data) {
                        console.log('Error:', data);
                    }
                });
             });
            
    });
        
    //******************END  XOA RECORD *********************
    
    
    
    //create new task / update existing task
    //****************** THÊM VÀ CẬP NHẬT RECORD *************************
    $(document).on('click','#btn-save',function(e) {
        
        e.preventDefault();
        // bay loi nhap vao 
       if( $('#txtTen').val()=="" || $('#txtDiachi').val()=="" || $('#txtSDT').val()=="" )
        {
            var str_alert = "<strong>Kiểm tra lại!</strong> Không được bỏ trống.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
                    
            return false;
        }
        
        
        // lay du lieu tu formvao , luu Y: Truong nhan du lieu vao la truong giong voi field trong table
        // trim(): xoa khoan trang du thua.
        var formData = {
            NCC_ID: id_ma,
            NCC_TEN: $('#txtTen').val().trim(),
            NCC_DIACHI: $('#txtDiachi').val().trim(),
            NCC_SDT: $('#txtSDT').val().trim(),
        }
        
        //used to determine the http verb to use [add=POST], [update=PUT]
        // kiểm tra thêm mới hay cập nhật
        var state = $('#btn-save').val(); // trong nút có attribute có value

        var type = 'POST'; //for creating new resource
        var id = $(id_ma).val(); // trong nút có id là id của record,
        var my_url = url;

        if (state == 'update'){
            type = 'PUT'; //for updating existing resource
            my_url = url + '/' + id;
        }
        console.log(formData);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        }); 
        $.ajax({
            type: type,
            url: my_url,            
            data: formData,           
            dataType: 'json',            
            success: function (data) {
                console.log(data);
                if(data.error=='idisset') // trường hợp trùng id
                {   
                    var str_alert = "<strong>Có rồi!</strong> Mã vừa nhập đã có  rồi hãy nhập mà mới.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
                    
                }
                else
                {
                    $(idfrm).trigger("reset");
                    $(idmodal).on('hidden.bs.modal',function(){
                    $('.right_col').load(url+'/all',function()
                    {
                            setDataTable('#datatable');
                            show_stack_bottomright('success');
                        });
                    });
                    
            $(idmodal).modal('hide');
                }// end kiểm tra tồn tại id  và thêm mới 
            }, // end   ajax  post xử lý dữ liệu xong
            error: function (data) {
                console.log('Error:', data); // thông báo lỗi qua console 
            }
        });
    });
    //******************END THÊM VÀ CẬP NHẬT RECORD *************************
});


$(document).ready(function(){

    var  url ="/public/nhanvien"; // dinh huong url  cho ajax
    var name ='BÁN HÀNG';
    var  idmodal = '#banhangModal';
    var  idfrm = '#frmBanHang';
    var  id_ma = '#chitietdonhang_id';
    var  id_TD =  '';

    if($('#today').val()=="") 
    {   

        //add datacombo        
        addDataForSelect('#cmbTuyenduong','/public/admin/tuyenduong/allresponse','TD_ID','TD_TEN','','');        

    }
    else
    {        
            var dt = JSON.parse($('#today').val());
            //add datacombo
            addDataForSelect('#cmbTuyenduong','/public/admin/tuyenduong/allresponse','TD_ID','TD_TEN',dt.ID_TD,'');
            addDataForSelect('#cmbKhachhang','/public/admin/khachhang/allresponse/'+dt.ID_TD,'KH_ID','KH_TEN',dt.ID_KH,'');
            setautoNumeric('.tiente','tiente');
        $('.tiente').autoNumeric('set',$('.tiente').html()); 
    }
    var  id_DH = taoIdDonhang('DH');
    
    showDay('#today');
    setautoNumeric('#txtSoluong','number');
    setautoNumeric('#txtDongia','tiente');  
    
    addDataForSelect('#cmbHanghoa','/public/admin/hanghoa/allresponse','HH_ID','HH_TEN','','QC_TEN');
    $(document).on('change','#cmbTuyenduong',function(){
            id_TD = $('#cmbTuyenduong').val();
            addDataForSelect('#cmbKhachhang','/public/admin/khachhang/allresponse/'+id_TD,'KH_ID','KH_TEN','','');
        });
    
    $(document).on('change','#cmbKhachhang',function(){
        
        id_KH = $('#cmbKhachhang').val();
        id_TD = $('#cmbTuyenduong').val();

      var  frmdata = {
            ID_KH: id_KH,
            ID_DH:  id_DH
        };
       $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        })
        $.ajax({
          url: url+'/chitietdonhang' ,
          type: 'post',
          dataType: 'json',
          data: frmdata,
          success: function (data){
              //console.log(data);
               $('.right_col').load(url+'/updatev/'+id_DH+'-'+id_KH,function(){
                            reloadAll(id_KH,id_TD);
                    });
          },
          error: function(data){
              
          }
            
        });
    });
    
    //  load dữ liệu vào modal form này.
    $(document).on('click','.open-modal',function(e){
       e.preventDefault();
        var id = $(this).val(); // id nằm trong btn có class .open-mal
        $.get(url + '/chitietdonhang/' + id, function (data) {
            console.log(data[0].DONGIA);
            $(id_ma).val(data[0].CTDH_ID); // ma chitietdonhang
            $('#txtTenhang').val(data[0].HH_TEN+' - '+data[0].QC_TEN);
            $('#txtSoluongedit').val(data[0].SOLUONG);            
            setautoNumeric('#txtDongiaedit','tiente');
            $('#txtDongiaedit').autoNumeric('set',data[0].DONGIA);
            $('#btn-save').val("update"); // chuyển value về trạng thái  'update'            
        }) ;
        $(idmodal).modal();
    });
    
    //display modal form for creating new task , ham them hang  vao chi tiet hang hoa
    $(document).on('click','#btnAdd',function(){
        
        if( $('#cmbTuyenduong').val()=="" || $('#cmbKhachhang').val()=="" || $('#cmbHanghoa').val()=="" || $('#txtSoluong').val()=="" || $('#txtDongia').autoNumeric('get')=="" || $('#Idnhanvien').val()=="")
        {
            
            show_stack_bottomright('error');
            return false;    
        }
        var id_KH = $('#cmbKhachhang').val();
        var id_TD = $('#cmbTuyenduong').val();
        var frmdata = {
            ID_HH: $('#cmbHanghoa').val() ,
            ID_DH: id_DH+'-'+id_KH,
            ID_KH: id_KH,
            SOLUONG: $('#txtSoluong').val(),
            DONGIA: $('#txtDongia').autoNumeric('get'),
        };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        })
        $.ajax({
           url: url+'/themchitietdonhang',
           type: 'post',
           dataType: 'json',
           data: frmdata,
           success:function(data){
                 $('.right_col').load(url+'/updatev/'+id_DH+'-'+id_KH,function(){
                            // setDataTable('#datatable');
                             show_stack_bottomright('success');                                                        
                            reloadAll(id_KH,id_TD);                      
                    });                   
               
           },
           error: function(data){
               console.log(data);
           }
           
            
        });
    });
    

    //delete task and remove it from list
    //****************** XOA RECORD *************************
    
    $(document).on('click','.delete',function(e){
        var id = $(this).val();
        var id_KH = $('#cmbKhachhang').val();
                                var id_TD = $('#cmbTuyenduong').val();
            $('#delModal').on('show.bs.modal', function(){
             $('#delModal').find('button.btnXoa').attr('id','btnxoa'+id);
             $('#delModal').find('.modal-title').text('THÔNG BÁO');
             $('#delModal').find('.modal-body').text('Bạn có thật sự muốn xóa');
           });
            $('#delModal').modal('show');
            $(document).on('click','#btnxoa'+id,function(e) {
                e.preventDefault();
                 $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                });
                $.ajax({
                    type: "DELETE",
                    url: url + '/chitietdonhang/' +id,
                    success: function (data) {
                      //  console.log(data);
                       $('#delModal').on('hidden.bs.modal',function(){
                            $('.right_col').load(url +'/updatev/'+id_DH + '-' + $('#cmbKhachhang').val(),function(){
                                
                                // setDataTable('#datatable');
                             show_stack_bottomright('success');

                              reloadAll(id_KH,id_TD);                            
                                });
                            });
                    
                     $("#delModal").modal("hide");
                    },
                    error: function (data) {
                        //console.log('Error:', data);
                    }
                });
             });
            
    });
        
    //******************END  XOA RECORD *********************

    //create new task / update existing task
    //****************** THÊM VÀ CẬP NHẬT RECORD *************************
    $(document).on('click','#btn-save',function(e) {
        
        e.preventDefault();
        // bay loi nhap vao 
       if( $('#txtTenhang').val()=="" || $('#txtSoluongedit').val()=="" || $('#txtDongiaedit').val()=="" )
        {
            var str_alert = "<strong>Kiểm tra lại!</strong> Không được bỏ trống.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
                    
            return false;
        }
        var alphaExp = /^[0-9a-zA-Z,.]+$/;
        // lay du lieu tu formvao , luu Y: Truong nhan du lieu vao la truong giong voi field trong table
        // trim(): xoa khoan trang du thua.
        var formData = {
            DONGIA: $('#txtDongiaedit').autoNumeric('get'),
            SOLUONG: $('#txtSoluongedit').val().trim(),
            
        }
        
        //used to determine the http verb to use [add=POST], [update=PUT]
        // kiểm tra thêm mới hay cập nhật
        var state = $('#btn-save').val(); // trong nút có attribute có value

        var type = 'POST'; //for creating new resource
        var id = $(id_ma).val(); // trong nút có id là id của record,
        var my_url = url;
        var id_KH = $('#cmbKhachhang').val();
        var id_TD =  $('#cmbTuyenduong').val(); 

        if (state == 'update'){
            type = 'PUT'; //for updating existing resource
            my_url = url + '/chitietdonhang/' + id;
        }
        //console.log(formData);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
            }
        }); 
        $.ajax({
            type: type,
            url: my_url,            
            data: formData,           
            dataType: 'json',            
            success: function (data) {
                //console.log(data);
                if(data.error=='idisset') // trường hợp trùng id
                {   
                    var str_alert = "<strong>Có rồi!</strong> Mã vừa nhập đã có  rồi hãy nhập mà mới.";
                    $(idmodal+' .modal-footer div.alert').html(str_alert);
                    $(idmodal+' .modal-footer div.alert').removeClass('hidden');
                    
                }
                else
                {
                    $(idfrm).trigger("reset");
                    $(idmodal).on('hidden.bs.modal',function(){
                    $(".right_col").load(url +"/updatev/"+id_DH +"-"+ $("#cmbKhachhang").val(),function()
                        {
                                
                                reloadAll(id_KH,id_TD);
                                show_stack_bottomright("success");
                        });
                    });
                    
                    $(idmodal).modal('hide');
                }// end kiểm tra tồn tại id  và thêm mới 
            }, // end   ajax  post xử lý dữ liệu xong
            error: function (data) {
                //console.log('Error:', data); // thông báo lỗi qua console 
            }
        });
    });
    //******************END THÊM VÀ CẬP NHẬT RECORD *************************
    
    
    $(document).on('click','#btn-huy',function(){
                   (new PNotify({
                        title: 'Xác nhận Hủy đơn hàng',
                        text: 'Bạn  muốn hủy không?',
                        icon: 'glyphicon glyphicon-question-sign',
                        styling: 'bootstrap3',
                        hide: false,
                        confirm: {
                            confirm: true
                        },
                        buttons: {
                            closer: false,
                            sticker: false
                        },
                        history: {
                            history: false
                        }
                    })).get().on('pnotify.confirm', function() {
                        //  click ok se huy 
                        var id_KH =  $('#cmbKhachhang').val();
                        var data = {
                            ID_DH : id_DH,
                            ID_KH : id_KH
                        }
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') // để thao tác được với laravel với http phải có token
                            }
                        });
                        $.ajax({
                            url: '/nhanvien/xoadonhang',
                            type: 'post',
                            data: data,
                            dataType: 'json',
                            success:function(data)
                            {
                                if(data.delete=="ok")
                                {
                                      window.location.href='/nhanvien/banhang';
                                }
                            },
                            error: function(data)
                            {},

                        });
                        
                    }).on('pnotify.cancel', function() {
                        
                    });
        
    }); //end functin  btn-huy
});


function reloadAll(id_KH,id_TD)
{
        showDay('#today'); // hien ngay thang 
        // set datatalbe cho bang bieu, bo di thuoc tinh xem 
        $('#datatable').DataTable({
                "language": {
                    "url": '/public/Vietnamese.json',
                },
                responsive: true,
                "sDom": 'f,t,i',                                    
            });
         // du lieu cho combo   
        addDataForSelect('#Idnhanvien','/public/admin/donhang/users','id','name','','');
        addDataForSelect('#cmbTuyenduong','/public/admin/tuyenduong/allresponse','TD_ID','TD_TEN',id_TD,'');
        addDataForSelect('#cmbTuyenduong','/public/admin/tuyenduong/allresponse','TD_ID','TD_TEN',id_TD,'');
        addDataForSelect('#cmbKhachhang','/public/admin/khachhang/allresponse/'+id_TD,'KH_ID','KH_TEN',id_KH,'');
        addDataForSelect('#cmbHanghoa','/public/admin/hanghoa/allresponse','HH_ID','HH_TEN','','QC_TEN');
        setautoNumeric('#txtSoluong','number');
        setautoNumeric('#txtDongia','tiente');
        setautoNumeric('.tiente','tiente');
        $('.tiente').autoNumeric('set',$('.tiente').html()); // say oh yeah
}
// ham su dataTable cho  bang du lieu
// truyen id cua bang vao
function setDataTable(idTable)
{
    
    $(idTable).DataTable({
        "language": {
            "url": '/public/Vietnamese.json',
        },
        responsive: true,
        
    });
}


//ham set thong bao, truyen gia tri type vao [success, error, info]
function show_stack_bottomright(type) {
 var stack_bottomright = {"dir1": "up", "dir2": "left", "firstpos1": 25, "firstpos2": 25};
    var opts = {
        title: "Ok! Thành Công",
        text: "Hệ thống đã lưu.",
        addclass: "stack-bottomright",
        styling: 'bootstrap3',
        icon: false,
        stack: stack_bottomright
        
    };
    switch (type) {
    case 'error':
        opts.title = "Oh No";
        opts.text = "Có lỗi xảy ra - Vui lòng kiểm tra lại những mục có * bắt buộc phải chọn hoặc nhập";
        opts.type = "error";
        break;
    case 'info':
        opts.title = "Tin mới";
        opts.text = "Kiểm tra trong index";
        opts.type = "info";
        break;
    case 'success':
        opts.title ="";
        opts.text = "<div class='text-center col-md-12'><i class='fa fa-check fa-4x center'></i></div>";
        opts.type = "success";
        break;
    }
    new PNotify(opts);
}
// hàm  thêm giá trị cho commbo search, ['id  select','url source','value','name shoq','valueid']
// valueid co gia tri ='', neu  khong chon select, neu co gia tri truyen vao thi select se  duoc selected ngay id do
function addDataForSelect(select,url,id, name, valueid, nameqc)
{
    
    $.get(url,function(data){
       //console.log(data);
        var db_select ="<option value=''>Chưa chọn</option>";
            for (var key in data) {
                if (data.hasOwnProperty(key)) {
                 //console.log(data[key]["TD_ID"] + ", " + data[key]["TD_TEN"]);
                    
                    var selected = "";
                    if (!valueid=="") {
                       if(valueid==data[key][id]) { selected ="selected='selected'";}
                    }
                        if(nameqc!='')
                        {
                         db_select+= "<option " + selected + " value='"+data[key][id]+"'>"+data[key][name]+'-'+data[key][nameqc]+"</option>";        
                        }
                        else
                        {
                            db_select+= "<option " + selected + " value='"+data[key][id]+"'>"+data[key][name]+"</option>";            
                        }
                        
                    
                     
                    }
            }
          //   console.log(db_select);
        $(select).html(db_select);
        if(valueid=="")
        {
            $(select).select2({
            placeholder: "-Chọn-",
            allowClear: true,
            dropdownAutoWidth : true,
            
            });
        }
        else
        {
            $(select).select2({
            allowClear: true,
            dropdownAutoWidth : true,
            });
            
        }
    });
    

}

// ham khoi tao id phieu xuat, phieu nhap
function taoIdPhieuNhapXuat(name)
{
var dateObj = new Date();
var month = dateObj.getUTCMonth() + 1; //months from 1-12
var day = dateObj.getUTCDate();
var year = dateObj.getUTCFullYear();
var h = dateObj.getHours();
var m = dateObj.getMinutes();
var s = dateObj.getSeconds();
newid = name+year + month + day+ '-' + h + m + s;
   return newid;
}


// ham khoi tao id phieu xuat, phieu nhap
function taoIdDonhang(name) 
{
var dateObj = new Date();

var month = dateObj.getUTCMonth() + 1; 
var day = dateObj.getUTCDate();
var year = dateObj.getUTCFullYear();
newid = name+year + month + day;
   return newid;
}

// ham tra ve gia tri thoi gian
function showDay(id)
{
    //hien ngay thang
    var dateObj = new Date();
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();
    var h = dateObj.getHours();
    var m = dateObj.getMinutes();
    var s = dateObj.getSeconds();
    newid =day + '/' +month + '/' + year ;
    $(id).html(newid);
}

//ham set autoNmeric input

function setautoNumeric(id,type)
{
    if(type=='tiente')
    {
        $(id).autoNumeric('init',{
        aSep: ',',
        aDec: '.', 
        aSign: 'đ',
        pSign:'l',
        vMin: '0',
        VMax:'9999999',
        mDec: '0',
        
        });    
    }
    else
    {
        $(id).autoNumeric('init',{
        aSep: '.',
        aDec: ',', 
        vMin: '0',
        vMax: '9999999999',
        mDec: '0'

        });    
    }
    
}